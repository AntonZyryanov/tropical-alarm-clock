//
//  alarm_clockApp.swift
//  alarm clock
//
//  Created by TonyMontana on 18.10.2021.
//

import SwiftUI
import UserNotifications
import AVFoundation

//import Amplitude
import ApphudSDK
//import AppTrackingTransparency
//import CoreData
//import FacebookCore
import Firebase
import FirebaseRemoteConfig
//import GoogleMobileAds
import UIKit
import AppTrackingTransparency
import AdSupport

@main
struct alarm_clockApp: App {
    let persistenceController = PersistenceController.shared

    @UIApplicationDelegateAdaptor private var appDelegate: AppDelegate
    
    var body: some Scene {
        WindowGroup {
           // OnBoarding().environment(\.managedObjectContext, persistenceController.container.viewContext)
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}


//*** Implement App delegate ***//
class AppDelegate: NSObject, UIApplicationDelegate {
    
    
   static let notificationCenter =  UNUserNotificationCenter.current()
    
      func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
       // requestPermission()
        Apphud.start(apiKey: "app_QuGvdr8LQYVxMsfYwx88JCv21nTP3W")
        
        PurchasesProcessings.loadProducts()
        AlarmAnalytics.facebookStart(application, launchOptions: launchOptions)
        AlarmAnalytics.analyticsSetup()
       // FirebaseApp.configure()
        AppDelegate.notificationCenter.requestAuthorization(options: [.alert,.sound]) { (granted, error) in
            
            guard granted else {return}
            
            AppDelegate.notificationCenter.getNotificationSettings { (settings) in
                guard settings.authorizationStatus == .authorized else {return}
                
                
            }
        }
        AppDelegate.notificationCenter.delegate = self
        //background music
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [.mixWithOthers, .allowAirPlay])
            
            try AVAudioSession.sharedInstance().setActive(true)
            
        } catch {
            print(error)
        }
 //    sendNotification()
     return true
    }
    
    
//No callback in simulator
//-- must use device to get valid push token
     func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)          {
      print(deviceToken)
    }
     func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
       print(error.localizedDescription)
    }
    
    
    //NEWLY ADDED PERMISSIONS FOR iOS 14
    static func requestPermission() {
        if #available(iOS 14, *) {
            ATTrackingManager.requestTrackingAuthorization { status in
                switch status {
                case .authorized:
                    // Tracking authorization dialog was shown
                    // and we are authorized
                    print("Authorized")
                    
                    // Now that we are authorized we can get the IDFA
                    print(ASIdentifierManager.shared().advertisingIdentifier)
                case .denied:
                    // Tracking authorization dialog was
                    // shown and permission is denied
                    print("Denied")
                case .notDetermined:
                    // Tracking authorization dialog has not been shown
                    print("Not Determined")
                case .restricted:
                    print("Restricted")
                @unknown default:
                    print("Unknown")
                }
            }
        }
    }
    //
    
    static func deleteNotification (requestName: Int,melody: String, repeatInterval: Int, alarmTime: String) {
        var requestsToDelete : [String] = []
        for requestIndex in 0...100{
            for miniRequestIndex in 0...7 {
                var request = String(requestName) + "_" + String(requestIndex) + "_" + String(miniRequestIndex)
                request = request + "_" + melody + "_" + String(repeatInterval) + "_" +  alarmTime
                requestsToDelete.append(request)
                print(request)
            }
            
        }
        AppDelegate.notificationCenter.removePendingNotificationRequests(withIdentifiers: requestsToDelete)
        
    }
    
    
    static func postponeNotification(alarmPrefix: String, alarmPostfix: String) {
        let userAction = "User action"
        
        let postponeAction = UNNotificationAction(identifier: "Postpone", title: "Postpone", options: [])
        
        let category = UNNotificationCategory(identifier: userAction, actions: [postponeAction], intentIdentifiers: [], options: [])
        
        notificationCenter.setNotificationCategories([category])
        
        
        var options = alarmPostfix.split(separator: "_")
        
        let content = UNMutableNotificationContent()
        content.title = "WAKE UP 👻"
        content.subtitle = String(options[2])
        content.body = "Tap to postpone alarm clock for \(options[1]) minutes"
        
        content.categoryIdentifier = userAction
        
        let melodyName = String(options[0]) + ".mp3"
        
        content.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: melodyName))
     //   let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false
        let year = Calendar.current.component(.year, from: Date())
        let month = Calendar.current.component(.month, from: Date())
        let day = Calendar.current.component(.day, from: Date())
        let hours = Calendar.current.component(.hour, from: Date())
        let minutes = Calendar.current.component(.minute, from: Date())
        
        var miniRequestsTimes: [Int] = [0, 4, 8 , 12, 16, 20, 24, 28, 32, 36 , 40 , 44 , 48, 52, 56]
        
        
        //creating request
        
        for index in 0..<1{
            var triggers : [UNTimeIntervalNotificationTrigger] = []
                  
            var requests : [UNNotificationRequest] = []
            
            for triggerNumber in 0...7 {
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: TimeInterval(exactly: Float(options[1])!*60+Float(miniRequestsTimes[triggerNumber]))!, repeats: false)
                triggers.append(trigger)
            }
            
            print("Request postponing...")
            for requestIndex in 0...7 {
                var requestID = alarmPrefix + "_" + String(requestIndex) + alarmPostfix
                print(requestID)
                let melodyName = String(options[0]) + String(requestIndex) + ".mp3"
                content.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: melodyName))
                let request = UNNotificationRequest(identifier: requestID, content: content, trigger: triggers[requestIndex])
                
                
                AppDelegate.notificationCenter.add(request) { (error) in
                    //print(error?.localizedDescription)
                    //print("shit")
                }
                
            }
        }
        
        
    }
    
    static func sendNotification (alarmID: Int,melody: String,repeatsMinutesIntervals: Int,alarmTime: String) {
        let userAction = "User action"
        
        let postponeAction = UNNotificationAction(identifier: "Postpone", title: "Postpone", options: [])
        
        let category = UNNotificationCategory(identifier: userAction, actions: [postponeAction], intentIdentifiers: [], options: [])
        
        notificationCenter.setNotificationCategories([category])
        
        let content = UNMutableNotificationContent()
        content.title = "WAKE UP 👻"
        content.subtitle = alarmTime
        content.body = "Open App to postpone"
        content.categoryIdentifier = userAction
        
        let melodyName = melody + ".mp3"
        content.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: melodyName))
        let hours = Int(alarmTime.prefix(2))
        let minutes = Int(alarmTime.suffix(2))
        
        var miniRequestsTimes: [Int] = [0, 4, 8 , 12, 16, 20, 24, 28, 32, 36 , 40 , 44 , 48, 52, 56]
        

        var firstHourMinutes : [Int] = []
        var secondHourMinutes : [Int] = []
        
        var nextDay: Date? = nil

        
        var minutesLimiter = minutes!
        while minutesLimiter < 60 {
            firstHourMinutes.append(minutesLimiter)
            minutesLimiter += repeatsMinutesIntervals
        }
        
        minutesLimiter = minutesLimiter % 60
        
        while minutesLimiter < 60 {
            secondHourMinutes.append(minutesLimiter)
            minutesLimiter += repeatsMinutesIntervals
        }
        
        
        var requestsCounter : Int = 0
        
        print("Setting new alarm...")
        
        for index in 0..<firstHourMinutes.count{
            var triggers : [UNCalendarNotificationTrigger] = []
                  
            var requests : [UNNotificationRequest] = []
            
            
            requestsCounter = index
            
            if requestsCounter < 8 {
                if requestsCounter == 7 {
                    content.body = "Tap to postpone alarm clock for \(repeatsMinutesIntervals) minutes"
                }
                for triggerNumber in 0...7 {
                    let trigger = UNCalendarNotificationTrigger(dateMatching: DateComponents(hour: hours, minute: firstHourMinutes[index], second: miniRequestsTimes[triggerNumber]), repeats: true)
                    triggers.append(trigger)
                }
                
                for requestIndex in 0...7 {
                    var requestID = String(alarmID) + "_" + String(requestsCounter) + "_" + String(requestIndex)
                    requestID = requestID + "_" + melody + "_" + String(repeatsMinutesIntervals) + "_" + alarmTime
                    print(requestID)
                    let melodyName = melody + String(requestIndex) + ".mp3"
                    content.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: melodyName))
                    let request = UNNotificationRequest(identifier: requestID, content: content, trigger: triggers[requestIndex])
                    
                    
                    AppDelegate.notificationCenter.add(request) { (error) in
                        
                    }
                    
                }
            }
            
           
        }
        
        
        
        var secondHour = hours! + 1
        if hours == 23 {
            secondHour = 0
        }
    
        
        for index in 0..<secondHourMinutes.count {
            
            var triggers : [UNCalendarNotificationTrigger] = []
                  
            var requests : [UNNotificationRequest] = []
            
            
            requestsCounter += 1
            
            if requestsCounter < 8 {
                if requestsCounter == 7 {
                    content.body = "Tap to postpone alarm clock for \(repeatsMinutesIntervals) minutes"
                }
                for triggerNumber in 0...7 {
                    let trigger = UNCalendarNotificationTrigger(dateMatching: DateComponents(hour: secondHour, minute: secondHourMinutes[index], second: miniRequestsTimes[triggerNumber]), repeats: true)
                    triggers.append(trigger)
                }
                
                for requestIndex in 0...7 {
                    var requestID = String(alarmID) + "_" + String(requestsCounter) + "_" + String(requestIndex)
                    requestID = requestID + "_" + melody + "_" + String(repeatsMinutesIntervals) + "_" + alarmTime
                    print(requestID)
                    let melodyName = melody + String(requestIndex) + ".mp3"
                    content.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: melodyName))
                    let request = UNNotificationRequest(identifier: requestID, content: content, trigger: triggers[requestIndex])
                    
                    AppDelegate.notificationCenter.add(request) { (error) in
                       
                    }
                    //experiment
                    
                }
            }
            
            
            
        }
        
        
        
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert,.sound])
       // print(#function)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
       // print(#function)
        switch response.actionIdentifier {
        case UNNotificationDismissActionIdentifier:
            postponeRequest(response: response)
        case UNNotificationDefaultActionIdentifier:
            postponeRequest(response: response)
        case "Postpone":
           postponeRequest(response: response)
        default:
            postponeRequest(response: response)
        }
        
        completionHandler()
    }
    
    
    func postponeRequest (response: UNNotificationResponse) {
        var notificationID = response.notification.request.identifier
        let idMargin = "_"
        let indicies = notificationID.indicesOf(string: idMargin)
        let numberOfSymbols = indicies[1] + 1
        var notificationsPrefix = notificationID.prefix(numberOfSymbols)
        var notificationPostfix = ""
        for index in indicies[2]...notificationID.count-1{
            notificationPostfix.append(notificationID[index])
        }
        var requestsToDelete : [String] = []
        for miniRequestID in 0...14{
            var miniRequest = notificationsPrefix + String(miniRequestID) + notificationPostfix
            requestsToDelete.append(String(miniRequest))
        }
        AppDelegate.notificationCenter.removePendingNotificationRequests(withIdentifiers: requestsToDelete)
        // ALARMS REPEAT
        var newAlarmIDString = ""
               for index in indicies[0]...indicies[1]{
            newAlarmIDString.append(notificationID[index])
            
        }
        newAlarmIDString.removeFirst()
        newAlarmIDString.removeLast()
        var newAlarmID = Int(newAlarmIDString)
        if newAlarmID! > 6 {
            newAlarmID! += 1
            var newAlarmPrefixNumberOfSymbols = indicies[0] + 1
            var newAlarmPrefix = notificationID.prefix(newAlarmPrefixNumberOfSymbols) + String(newAlarmID!)
            AppDelegate.postponeNotification(alarmPrefix: String(newAlarmPrefix), alarmPostfix: notificationPostfix)
        }
    }
    
    
}

//Facebook SDK extension
extension AppDelegate {
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        let app = options[.sourceApplication] as? String
        //  VK.handle(url: url, sourceApplication: app)

        return true
    }
}

extension AppDelegate {
    static let shared = UIApplication.shared.delegate as! AppDelegate
}

// extension for Postpone alarm action
extension String {
    func indicesOf(string: String) -> [Int] {
        var indices = [Int]()
        var searchStartIndex = self.startIndex

        while searchStartIndex < self.endIndex,
            let range = self.range(of: string, range: searchStartIndex..<self.endIndex),
            !range.isEmpty
        {
            let index = distance(from: self.startIndex, to: range.lowerBound)
            indices.append(index)
            searchStartIndex = range.upperBound
        }

        return indices
    }
}


extension StringProtocol {
    subscript(offset: Int) -> Character {
        self[index(startIndex, offsetBy: offset)]
    }
}
