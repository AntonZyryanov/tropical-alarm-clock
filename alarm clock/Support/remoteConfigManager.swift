//
//  remoteConfigManager.swift
//  alarm clock
//
//  Created by TonyMontana on 28.11.2021.
//

import Foundation
import SwiftUI
import FirebaseRemoteConfig
import Foundation

class RemoteConfigManager {
    
    static let shared = RemoteConfigManager()
    
    
    private init () {
        
    }
    
    func fetchData () {
        let remoteConfig = RemoteConfig.remoteConfig()
        let settings = RemoteConfigSettings()
        settings.minimumFetchInterval = 0
        remoteConfig.configSettings = settings
        
        remoteConfig.fetchAndActivate { status, error in
            if error != nil {
                print(error!.localizedDescription)
            } else {
                if status != .error {
                    if let appStoreLink =
                        remoteConfig["appStoreLink"].stringValue
                    {
                        UserDefaults.standard.setValue(appStoreLink, forKey: "appStoreLink")
                    }
                }
            }
        }
    }
    
    
    
}
