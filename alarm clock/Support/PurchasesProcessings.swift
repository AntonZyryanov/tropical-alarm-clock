//
//  PurchasesProcessings.swift
//  alarm clock
//
//  Created by TonyMontana on 18.11.2021.
//

import ApphudSDK
import Foundation
import StoreKit

class PurchasesProcessings {
    static var productsLoaded: Bool = false
    
    static var shared = PurchasesProcessings()
    
    static var paywall: ApphudPaywall?
    static var products: [ApphudProduct]?
    
    var nc = NotificationCenter.default
    
    private init() {
   /*     self.nc.addObserver(self, selector: #selector(self.productsLoaded), name: Notification.Name("productsLoaded"), object: nil)   */
    }

    static func loadProducts() {
      /*  Apphud.getPaywalls { paywalls, error in
            if error == nil {
                // retrieve current paywall with identifier
                self.paywall = paywalls?.first(where: { $0.identifier == "Subscriptions" })
                // retrieve the products [ApphudProduct] from current paywall
                self.products = self.paywall?.products
                let nc = NotificationCenter.default
                nc.post(name: Notification.Name("productsLoaded"), object: nil)
                self.productsLoaded = true
            }
        }   */
        
        Apphud.paywallsDidLoadCallback { (paywalls) in
            // retrieve current paywall with identifier
            self.paywall = paywalls.first(where: { $0.identifier == "Subscriptions" })
            // retrieve the products [ApphudProduct] from current paywall
            self.products = self.paywall?.products
            let nc = NotificationCenter.default
            nc.post(name: Notification.Name("productsLoaded"), object: nil)
            print("Purchases processings products loaded")
            self.productsLoaded = true
        }
    }
    
    
}


