//
//  Browser.swift
//  alarm clock
//
//  Created by TonyMontana on 23.11.2021.
//

import SwiftUI

struct Browser: View {
    
    @Binding var isPrvacyPolicyChosen: Bool
    
/*    init(isPrvacyPolicyChosen:Bool) {
        self.isPrvacyPolicyChosen = isPrvacyPolicyChosen
    }   */
    
    var body: some View {
        if isPrvacyPolicyChosen{
            SFSafariViewWrapper(url: URL(string: "https://docs.google.com/document/d/1xqq77QineRpBd931cEidl9ZrbjIRqc9b/edit?usp=sharing&ouid=115761997364755810900&rtpof=true&sd=true")!)
        }else{
            SFSafariViewWrapper(url: URL(string: "https://docs.google.com/document/d/1rV-w7Rlx7xCeB78VS4rm3ZkrUEngYUyQ/edit?usp=sharing&ouid=115761997364755810900&rtpof=true&sd=true")!)
        }
        
    }
}

struct Browser_Previews: PreviewProvider {
    static var previews: some View {
       // Browser()
        Text("")
    }
}
