//
//  AlarmAnalytics.swift
//  alarm clock
//
//  Created by TonyMontana on 19.11.2021.
//



import Amplitude
import FacebookCore
import Firebase
import Foundation
import UIKit

class AlarmAnalytics {
    static let amplitude = Amplitude()

    enum Events: String {
        case firstOnBoardingPassed = "first_onboarding_passed"
        case secondOnBoardingPassed = "second_onboarding_passed"
        case thirdOnBoardingPassed = "third_onboarding_passed"
        case paywallPassed = "paywall_shown"
        case subscribeButtonPressed = "subscribe_button_pressed"
        case userTappedOnBanner = "user_tapped_on_banner"
        case interstitialShown = "interstitial_shown"
        case mainVCSettingsButtonPressed = "settings_button_pressed"
        case mainVCGalleryButtonPressed = "gallery_button_pressed"
        case mainVCPhotoshootButtonPressed = "photoshoot_button_pressed"
        case effectsLibraryOpened = "effects_library_opened"
        case saveButtonPressed = "save_button_pressed"
        case yearSubscription = "year_subscription_button_pressed"
        case monthSubscription = "month_subscription_button_pressed"
        case alarmsShown = "alarms_shown"
        case melodiesShown = "melodies_shown"
        case dreamHelpersShown = "dream_helpers_shown"
        case stopwatchShown = "stopwatch_shown"
        case settingsShown = "settings_shown"
    }

    static func analyticsSetup() {
        print("ANALYTICS SETUP")
        // Enable sending automatic session events
         amplitude.trackingSessionEvents = true
         // Initialize SDK
      //   amplitude.initializeApiKey("API_KEY")
         // Set userId
      //   amplitude.setUserId("userId")
         // Log an event
        // amplitude.logEvent("app_start")
        FirebaseApp.configure()
        AlarmAnalytics.amplitude.initializeApiKey("5227b7d9ab42eb4d43139e317c99ec93")
    }

    static func analyticsEvent(event: String) {
        AlarmAnalytics.amplitude.logEvent(event)
        Analytics.logEvent(event, parameters: nil)
      //  FacebookCore.AppEvents.logEvent(AppEvents.Name(rawValue: event))
    }

    static func analyticsEventWithProperties(event: String, properties: [String: Any]) {
        AlarmAnalytics.amplitude.logEvent(event, withEventProperties: properties)
        Analytics.logEvent(event, parameters: properties)
    }

    static func facebookStart(_ application: UIApplication, launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
   /*     FacebookCore.ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )*/
   //     FacebookCore.Settings.setDataProcessingOptions(["LDU"], country: 0, state: 0)
     //   FacebookCore.Settings.isAutoLogAppEventsEnabled = true
        //FacebookCore.AppEvents.activateApp()
    }
}


