//
//  Paywall.swift
//  alarm clock
//
//  Created by TonyMontana on 27.10.2021.
//

import SwiftUI
import ApphudSDK
import SafariServices
import AppTrackingTransparency
import AdSupport

struct Paywall: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @State var isYearSubChosen : Bool = true
    
    @State private var showTermsAndConditions: Bool = false
    
    @State private var showPrivacyPolicy: Bool = false
    
    @State private var isPrvacyPolicyChosen: Bool = false
    
    @State var link: String = "https://www.apple.com"
    
    @State var productsLoaded: Bool = false
    
    let pub = NotificationCenter.default
                .publisher(for: NSNotification.Name("productsLoaded"))
    
    var body: some View {
        
        
        ZStack{
            Color.black.ignoresSafeArea()
            VStack{//was scrollView
                
                Spacer().frame(height: 10)
                    HStack{
                        Spacer().frame(width: 20)
                        Button(action: {
                            ATTrackingManager.requestTrackingAuthorization { status in
                                switch status {
                                case .notDetermined:
                                    break
                                case .restricted:
                                    break
                                case .denied:
                                    break
                                case .authorized:
                                    break
                                @unknown default:
                                    break
                                }
                            }
                            self.presentationMode.wrappedValue.dismiss()
                                        }) {
                            VStack{
                                HStack{
                                    Image(systemName: "xmark")
                                        .foregroundColor(.pink)
                                    Spacer().frame(width: 8)
                                }
                                Spacer().frame(height: 10)
                            }
                            
                    }
                        Spacer()
                        Button(action: {
                            Apphud.restorePurchases { _, _, _ in
                                if Apphud.hasActiveSubscription() {
                                    // has active subscription
                                } else {
                                    // no active subscription found, check non-renewing purchases or error
                                }
                            }
                                        }) {
                            Text("RESTORE").foregroundColor(Color.pink)
                                .font(Font.custom("DINCondensed-Bold", size: 20))
                                .shadow(color: .pink, radius: 6, x: -1, y: -1)
                    }
                        Spacer().frame(width: 20)
                    }.frame(height:20)
                    Spacer().frame(height: 10)
                    HStack{
                       // Image("stripperLittleLogo")
                        Spacer().frame(width: 20)
                        VStack
                        {
                        Text("GET FULL VERSION NOW").foregroundColor(Color.pink)
                            .font(Font.custom("DINCondensed-Bold", size: 60))
                            .shadow(color: .pink, radius: 6, x: -1, y: -1)
                        Spacer()
                            }
                        Spacer()
                        VStack{
                            Image("paywallPalm")
                            Spacer()
                        }
                        Spacer().frame(width:50)
                        
                    }.frame(height: 200)
                    VStack{
                        HStack{
                          //  Spacer().frame(width: 40)
                            Text("NO ADVERTISEMENT").foregroundColor(Color.pink)
                                .font(Font.custom("DINCondensed-Bold", size: 30))
                                .shadow(color: .pink, radius: 6, x: -1, y: -1)
                        }
                        HStack{
                            //Spacer().frame(width: 40)
                            Text("ADDITIONAL MELODIES").foregroundColor(Color.pink)
                                .font(Font.custom("DINCondensed-Bold", size: 30))
                                .shadow(color: .pink, radius: 6, x: -1, y: -1)
                        }
                        HStack{
                           // Spacer().frame(width: 40)
                            Text("ADDITIONAL DREAM HELPERS").foregroundColor(Color.pink)
                                .font(Font.custom("DINCondensed-Bold", size: 30))
                                .shadow(color: .pink, radius: 6, x: -1, y: -1)
                        }
                        Spacer().frame(height: 10)
                        Button(action: {
                            if self.isYearSubChosen == false{
                                self.isYearSubChosen = true
                            }
                                        }) {
                            HStack{
                                Spacer().frame(width: 20)
                                if productsLoaded{
                                    
                                    
                                    if (PurchasesProcessings.products?[0].skProduct?.price) != nil {
                                        if (PurchasesProcessings.products?[0].skProduct?.priceLocale.currencySymbol) != nil {
                                            Text("1 MONTH FOR \((PurchasesProcessings.products?[0].skProduct!.price)!) \((PurchasesProcessings.products?[0].skProduct!.priceLocale.currencySymbol)!)").foregroundColor(Color.pink)
                                                .font(Font.custom("DINCondensed-Bold", size: 30))
                                                .shadow(color: .pink, radius: 6, x: -1, y: -1)
                                        }
                                    }else {
                                        Text("1 MONTH FOR ...").foregroundColor(Color.pink)
                                            .font(Font.custom("DINCondensed-Bold", size: 30))
                                            .shadow(color: .pink, radius: 6, x: -1, y: -1)
                                    }
                                    
                                    
                                }else{
                                    Text("1 MONTH FOR ...").foregroundColor(Color.pink)
                                        .font(Font.custom("DINCondensed-Bold", size: 30))
                                        .shadow(color: .pink, radius: 6, x: -1, y: -1)
                                }
                                
                                Spacer()
                                VStack{
                                    Spacer().frame(height:5)
                                    Text("FREE FOR 3").foregroundColor(Color.yellow)
                                        .font(Font.custom("DINCondensed-Bold", size: 20))
                                        .shadow(color: .yellow, radius: 6, x: -1, y: -1)
                                    Text("DAYS").foregroundColor(Color.yellow)
                                        .font(Font.custom("DINCondensed-Bold", size: 20))
                                        .shadow(color: .yellow, radius: 6, x: -1, y: -1)
                                }
                               
                                if self.isYearSubChosen {
                                    Image("paywallCBOn")
                                }else {
                                    Image("paywallCBOff")
                                }
                                Spacer().frame(width: 20)
                            }.frame(height: 40)
                    }
                        
                        Spacer().frame(height: 10)
                        Button(action: {
                            if self.isYearSubChosen == true{
                                self.isYearSubChosen = false
                            }
                                        }) {
                            HStack{
                                Spacer().frame(width: 20)
                                if productsLoaded{
                                    
                                    if (PurchasesProcessings.products?[1].skProduct?.price) != nil {
                                        if (PurchasesProcessings.products?[1].skProduct?.priceLocale.currencySymbol) != nil {
                                            Text("1 WEEK FOR \((PurchasesProcessings.products?[1].skProduct!.price)!) \((PurchasesProcessings.products?[1].skProduct!.priceLocale.currencySymbol)!)").foregroundColor(Color.pink)
                                                .font(Font.custom("DINCondensed-Bold", size: 30))
                                                .shadow(color: .pink, radius: 6, x: -1, y: -1)
                                        }
                                    }else {
                                        Text("1 WEEK FOR ...").foregroundColor(Color.pink)
                                            .font(Font.custom("DINCondensed-Bold", size: 30))
                                            .shadow(color: .pink, radius: 6, x: -1, y: -1)
                                    }
                                    
                                    
                                }else{
                                    Text("1 WEEK FOR ...").foregroundColor(Color.pink)
                                        .font(Font.custom("DINCondensed-Bold", size: 30))
                                        .shadow(color: .pink, radius: 6, x: -1, y: -1)
                                }
                                
                               
                                Spacer()
                                if self.isYearSubChosen {
                                    Image("paywallCBOff")
                                }else {
                                    Image("paywallCBOn")
                                }
                                Spacer().frame(width: 20)
                            }.frame(height: 40)
                            
                    }
                        
                        Spacer().frame(height: 10)
                        
                        Text("You will be charged automatically after 3 day trial period. You can delete subscription at any time. If you chosen monthly subscription, you will be charged every month. If you chosen annual subscription, you will be charged every year.").foregroundColor(Color.pink)
                            .font(Font.custom("DINCondensed-Bold", size: 8))
                            .shadow(color: .pink, radius: 6, x: -1, y: -1)
                            .padding()
                            .frame(height: 50)
                        VStack{
                            VStack{
                                if productsLoaded{
                                    Text("").frame(height:20)
                                }else{
                                    HStack{
                                        ProgressView().progressViewStyle(CircularProgressViewStyle(tint: Color.yellow))
                                        Spacer().frame(width: 10)
                                        VStack{
                                            Spacer().frame(height:5)
                                            Text("PRODUCTS LOADING...").foregroundColor(Color.yellow)
                                                .font(Font.custom("DINCondensed-Bold", size: 20))
                                                .shadow(color: .yellow, radius: 6, x: -1, y: -1)
                                        }
                                        Spacer().frame(width:5)
                                        
                                    }
                                    .frame(height:20)
                                    .onReceive(pub) { (output) in
                                        self.loadProducts()
                                    }
                                    
                                }
                            }.frame(height:20)
                            
                          //  Spacer()//.frame(height:10)
                            HStack{
                                Spacer()
                                Image("shoppingBasket")
                               // Spacer().frame(width: 20)
                                VStack{
                                    Spacer().frame(height: 20)
                                    Button(action: {
                                        if self.isYearSubChosen == true {
                                            Apphud.purchase(PurchasesProcessings.products![0]) { result in
                                                if let subscription = result.subscription, subscription.isActive() {
                                                    // has active subscription
                                                    AlarmAnalytics.analyticsEvent(event: AlarmAnalytics.Events.yearSubscription.rawValue)
                                                    
                                                    let nc = NotificationCenter.default
                                                    nc.post(name: Notification.Name("userBoughtSubscription"), object: nil)
                                                } else if let purchase = result.nonRenewingPurchase, purchase.isActive() {
                                                    // has active non-renewing purchase
                                                } else {
                                                    // handle error or check transaction status.
                                                }
                            
                                            }
                                        } else {
                                            Apphud.purchase(PurchasesProcessings.products![1]) { result in
                                                if let subscription = result.subscription, subscription.isActive() {
                                                    // has active subscription
                                                    AlarmAnalytics.analyticsEvent(event: AlarmAnalytics.Events.monthSubscription.rawValue)
                                                    
                                                    let nc = NotificationCenter.default
                                                    nc.post(name: Notification.Name("userBoughtSubscription"), object: nil)
                                                } else if let purchase = result.nonRenewingPurchase, purchase.isActive() {
                                                    // has active non-renewing purchase
                                                } else {
                                                    // handle error or check transaction status.
                                                }
                                              
                                            }
                                        }
                                                    }) {
                                        Text("SUBSCRIBE").foregroundColor(Color.yellow)
                                            .font(Font.custom("DINCondensed-Bold", size: 30))
                                            .shadow(color: .yellow, radius: 6, x: -1, y: -1)
                                            
                                    }.allowsHitTesting(self.productsLoaded)
                                    .frame(height:50)
                                }.frame(height: 70)
                                Spacer().frame(width: 80)
                                Spacer()
                               
                            }.frame(height: 60)
                           // Spacer()//.frame(height: 25)
                            
                            Spacer()
                        }
                        
                       
                    }
                
                
                    
            }.navigationBarTitle("")
            .navigationBarHidden(true)
            .background(Image("paywallBG2"))
            .fullScreenCover(isPresented: $showTermsAndConditions, content: {
               
                Browser(isPrvacyPolicyChosen: $isPrvacyPolicyChosen)
            })
            .onAppear{
                AlarmAnalytics.analyticsEvent(event: AlarmAnalytics.Events.paywallPassed.rawValue)
                if  PurchasesProcessings.productsLoaded {
                    self.productsLoaded = true
                }
              //  AppDelegate.requestPermission()
            }
            Spacer()
            VStack{
                Spacer()
                HStack{
                    Spacer().frame(width: 20)
                    Button(action: {
                        self.isPrvacyPolicyChosen = true
                        
                            self.showTermsAndConditions = true
                                    }) {
                        Text("PRIVACY POLICY").foregroundColor(Color.pink)
                            .font(Font.custom("DINCondensed-Bold", size: 20))
                            .shadow(color: .pink, radius: 6, x: -1, y: -1)
                            .frame(height:30)
                }
                    Spacer()
                    Button(action: {
                        self.isPrvacyPolicyChosen = false
                     
                            self.showTermsAndConditions = true
                                
                                    }) {
                        Text("TERMS AND CONDITIONS").foregroundColor(Color.pink)
                            .font(Font.custom("DINCondensed-Bold", size: 20))
                            .shadow(color: .pink, radius: 6, x: -1, y: -1)
                            .frame(height:30)
                }
                    Spacer().frame(width: 20)
                }.frame(height:30)
                Spacer().frame(height: 20)
            }
        }
            
            
    
    }
    
    
    func loadProducts() {
        print("Products loaded")
        self.productsLoaded = true
        }
}

struct Paywall_Previews: PreviewProvider {
    static var previews: some View {
        Paywall()
    }
}


struct SFSafariViewWrapper: UIViewControllerRepresentable {
    let url: URL

    func makeUIViewController(context: UIViewControllerRepresentableContext<Self>) -> SFSafariViewController {
        return SFSafariViewController(url: url)
    }

    func updateUIViewController(_ uiViewController: SFSafariViewController, context: UIViewControllerRepresentableContext<SFSafariViewWrapper>) {
        return
    }
}
