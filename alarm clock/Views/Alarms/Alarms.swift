//
//  Alarms.swift
//  alarm clock
//
//  Created by TonyMontana on 19.10.2021.
//

import SwiftUI

struct Alarms: View {
    
    init(){
        UITableView.appearance().backgroundColor = .black
        UITableView.appearance().rowHeight = 100
        UITabBar.appearance().isHidden = true
        UITabBar.appearance().barTintColor = UIColor.purple
        UIBarButtonItem.appearance().tintColor = UIColor.purple
        UITabBar.appearance().backgroundColor = UIColor.black
        UINavigationBar.appearance().backgroundColor = UIColor.black
        UINavigationBar.appearance().barTintColor = UIColor.black
        UINavigationBar.appearance().isUserInteractionEnabled = false
    }
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @State private var showingPopover = false
    
    @State var allAlarms: [Alarm] = []
    
    @State var activeAlarms: [Alarm] = []
    
    @State var switchesFix: [Bool] = []
    
    @State var isEditing : Bool = false
    
    @State var showingPermissionAlert: Bool = false
    
    var body: some View {
        ZStack{
            Color.black.ignoresSafeArea()
            VStack{
                
                
                
                ScrollView{
                    
                    HStack{
                        Spacer().frame(width: 20)
                        Button(action: {
                            presentationMode.wrappedValue.dismiss()
                                        }) {
                            HStack{
                                Image(systemName: "arrow.left")
                                    .foregroundColor(.purple)
                                Spacer().frame(width: 8)
                            }.frame(width: 20, height: 20)
                            Spacer().frame(height: 40)
                            
                    }.frame(width: 20, height: 20)
                        Spacer()
                        Button(action: {
                            if self.isEditing == false {
                                self.isEditing = true
                            }else{
                                self.isEditing = false
                            }
                                        }) {
                            HStack{
                                Image(systemName: "pencil")
                                    .foregroundColor(.purple)
                                Spacer().frame(width: 8)
                            }.frame(width: 20, height: 20)
                            Spacer().frame(height: 40)
                            
                        }.animation(.spring())
                        .frame(width: 20, height: 20)
                        Spacer().frame(width:10)
                        NavigationLink(destination: AddAlarm(allAlarms: $allAlarms,switchesFix: $switchesFix)){
                            ZStack{
                                Image(systemName: "plus")
                                    .foregroundColor(.purple)
                            }.frame(width: 20, height: 20)
                            
                        }.frame(width: 20, height: 20)
                        Spacer().frame(width: 20)
                    }
                    
                    Spacer().frame(height: 40)
                    HStack{
                       // Image("stripperLittleLogo")
                        Spacer().frame(width: 20)
                        VStack
                        {
                        Text("ALARM").foregroundColor(Color.purple)
                            .font(Font.custom("DINCondensed-Bold", size: 60))
                            .shadow(color: .purple, radius: 6, x: -1, y: -1)
                            /*    Text("CLOCK").foregroundColor(Color.purple)
                                .font(Font.custom("DINCondensed-Bold", size: 60))
                            .shadow(color: .purple, radius: 6, x: -1, y: -1)    */
                        Spacer()
                            }
                        Spacer()
                        VStack{
                            Image("alarmClockNew")
                            Spacer().frame(height:60)
                        }
                        Spacer().frame(width: 20)
                        
                    }.frame(height:120)
                    
                    ForEach(allAlarms,id:\.id){ alarm in
                        HStack{
                            VStack{
                                Spacer().frame(height:25)
                                Text(alarm.time).foregroundColor(Color.purple)
                                    .font(Font.custom("DBLCDTempBlack", size: 40))
                                    .shadow(color: .purple, radius: 6, x: -1, y: -1)
                                    .lineLimit(1)
                                    .frame(width: 150)
                                Spacer().frame(height:5)
                       /*         HStack{
                                    Spacer().frame(width: 20)
                                    Text(alarm.alarmDate).foregroundColor(Color.purple)
                                        .font(Font.custom("DBLCDTempBlack", size: 10))
                                        .shadow(color: .purple, radius: 6, x: -1, y: -1)
                                        .lineLimit(1)
                                        .frame(width: 50)
                                    Spacer()
                                }   */
                                
                            }
                            
                            Spacer().frame(height:100)
                            HStack{
                                if self.isEditing == true{
                                    VStack{
                                        Spacer().frame(height: 10)
                                        
                                            Toggle("", isOn: $switchesFix[alarm.id]).toggleStyle(SwitchToggleStyle(tint: .purple)).onChange(of: switchesFix[alarm.id]){ (toggle) in
                                                 if toggle == true{
                                                    AppDelegate.sendNotification(alarmID: alarm.alarmID, melody: alarm.melody, repeatsMinutesIntervals: alarm.timeInterval, alarmTime: alarm.time)
                                                    self.allAlarms[alarm.id].isOn = true
                                                 }else{
                                                     print("alarm deleted")
                                                    self.allAlarms[alarm.id].isOn = false
                                                    AppDelegate.deleteNotification(requestName: alarm.alarmID,melody: alarm.melody,repeatInterval: alarm.timeInterval,alarmTime: alarm.time)
                                                 }
                                             }
                                        
                                        
                                        
                                    }
                                    
                                    
                                    Button(action: {
                                        alarm.isOn = false
                                                let deletedElement = alarm.id
                                        
                                      //  AppDelegate.deleteNotification(requestName: alarm.alarmID,melody: alarm.melody,repeatInterval: alarm.timeInterval,alarmTime: alarm.time)
                                        print("alarm deleted")
                                        self.allAlarms[alarm.id].isOn = false
                                        AppDelegate.deleteNotification(requestName: alarm.alarmID,melody: alarm.melody,repeatInterval: alarm.timeInterval,alarmTime: alarm.time)
                                                
                                                allAlarms.remove(at: alarm.id)
                                            
                                                for editedAlarm in allAlarms {
                                                    if editedAlarm.id > deletedElement{
                                                        editedAlarm.id -= 1
                                                    }
                                                    
                                                }
                                                
                                        updateActiveAlarms(allAlarms: self.allAlarms)
                                                
                                                            }) {
                                        VStack{
                                            Spacer().frame(height:5)
                                            HStack{
                                             Image(systemName: "trash.fill")
                                                    .foregroundColor(.purple)
                                                Spacer().frame(width: 8)
                                            }.frame(minWidth: 0, maxWidth: 40, minHeight: 0, maxHeight: 40)
                                            
                                        }
                                                
                                                
                                                
                                            }.frame(width: 40, height: 40)
                                    Spacer().frame(width: 30)
                                }else{
                                    VStack{
                                        Spacer().frame(height: 10)
                                        
                                            Toggle("", isOn: $switchesFix[alarm.id]).toggleStyle(SwitchToggleStyle(tint: .purple)).onChange(of: switchesFix[alarm.id]){ (toggle) in
                                                 if toggle == true{
                                           
                                                    self.allAlarms[alarm.id].isOn = true
                                                 }else{
                                                     print("alarm deleted")
                                                    self.allAlarms[alarm.id].isOn = false
                                                    AppDelegate.deleteNotification(requestName: alarm.alarmID,melody: alarm.melody,repeatInterval: alarm.timeInterval,alarmTime: alarm.time)
                                                 }
                                                updateActiveAlarms(allAlarms: self.allAlarms)
                                             }
                                        
                                        
                                        
                                    }
                                    
                                    Spacer().frame(width: 40)
                                }
                            }.animation(.linear(duration: 0.3))
                           
                            
                                
                                
                            
                           
                        }//.listRowBackground(Color.black)
                    }.onDelete(perform: delete)
                }
                Spacer().frame(height:20)
            }
            
        }.navigationBarTitle("")
        .navigationBarHidden(true)
        .onDisappear {
            
            var exportAllAlarmsID : [Int] = []
            var exportAllAlarmsTime : [String] = []
            var exportAllAlarmsSwitches : [Bool] = []
            var exportAllAlarmsMelodies : [String] = []
            var exportAllAlarmsTimeIntervals : [Int] = []
            for index in 0..<allAlarms.count {
                exportAllAlarmsID.append(allAlarms[index].id)
                exportAllAlarmsTime.append(allAlarms[index].time)
                exportAllAlarmsSwitches.append(allAlarms[index].isOn)
                exportAllAlarmsMelodies.append(allAlarms[index].melody)
                exportAllAlarmsTimeIntervals.append(allAlarms[index].timeInterval)
            }
            
            UserDefaults.standard.setValue(exportAllAlarmsID, forKey: "allAlarmsID")
            UserDefaults.standard.setValue(exportAllAlarmsTime, forKey: "allAlarmsTime")
            UserDefaults.standard.setValue(exportAllAlarmsSwitches, forKey: "allAlarmsSwitches")
            UserDefaults.standard.setValue(exportAllAlarmsMelodies, forKey: "allAlarmsAlarmsMelodies")
            UserDefaults.standard.setValue(exportAllAlarmsTimeIntervals, forKey: "allAlarmsAlarmsTimeIntervals")
            updateActiveAlarms(allAlarms: self.activeAlarms)
        }
        .alert(isPresented:$showingPermissionAlert) {
                
            Alert(title: Text("Please turn Notifications on in Settings to use alarms"),
                message: Text(""),
                dismissButton: Alert.Button.default(
                    Text("Go to settings"), action: { UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!) }
                )
            )
            }
        .onAppear {
            print("OnAppear")
            notificationAuthorization()
            AlarmAnalytics.analyticsEvent(event: AlarmAnalytics.Events.alarmsShown.rawValue)
            
            let allAlarmsID = UserDefaults.standard.object(forKey: "allAlarmsID") as? [Int]
            let allAlarmsTime = UserDefaults.standard.object(forKey: "allAlarmsTime") as? [String]
            
            let allAlarmsAlarmsID = UserDefaults.standard.object(forKey: "allAlarmsAlarmsID") as? [Int]
            
            let allAlarmsAlarmsDate = UserDefaults.standard.object(forKey: "allAlarmsAlarmsDate") as? [String]
            
            let allAlarmsSwitches = UserDefaults.standard.object(forKey: "allAlarmsSwitches") as? [Bool]
            let allAlarmsAlarmsMelodies = UserDefaults.standard.object(forKey: "allAlarmsAlarmsMelodies") as? [String]
            
            let allAlarmsAlarmsTimeIntervals = UserDefaults.standard.object(forKey: "allAlarmsAlarmsTimeIntervals") as? [Int]
            
            if allAlarmsID == nil {
                
                self.allAlarms = []
            }else {
                
                var donwloadedAlarms : [Alarm] = []
                var switchesFixArray : [Bool] = []
                for index in 0..<allAlarmsID!.count {
                    
                    donwloadedAlarms.append(Alarm(id: allAlarmsID![index], time: allAlarmsTime![index], alarmID: allAlarmsAlarmsID![index], alarmDate: allAlarmsAlarmsDate![index], isOn: allAlarmsSwitches![index],melody: allAlarmsAlarmsMelodies![index], timeInterval: allAlarmsAlarmsTimeIntervals![index]))
                    donwloadedAlarms[index].isOn = allAlarmsSwitches![index]
                    switchesFixArray.append(allAlarmsSwitches![index])
                }
                
                self.allAlarms = donwloadedAlarms
                self.switchesFix = switchesFixArray
            }
            updateActiveAlarms(allAlarms: allAlarms)
        }
        
    }
    
    func notificationAuthorization() {
        AppDelegate.notificationCenter.requestAuthorization(options: [.alert,.sound]) { (granted, error) in
            
            guard granted else {
                self.showingPermissionAlert = true
                return
            }
            
            AppDelegate.notificationCenter.getNotificationSettings { (settings) in
                print(settings)
                guard settings.authorizationStatus == .authorized else {
                    return }
                
                
            }
        }
    }
    
     func delete(at offsets: IndexSet) {
           // allAlarms.remove(atOffsets: offsets)
        //
        var exportAllAlarmsID : [Int] = []
        var exportAllAlarmsTime : [String] = []
        var exportAllAlarmsSwitches : [Bool] = []
        var count = allAlarms.count - 1
        for index in 0..<count {
            exportAllAlarmsID.append(allAlarms[index].id)
            exportAllAlarmsTime.append(allAlarms[index].time)
            exportAllAlarmsSwitches.append(allAlarms[index].isOn)
        }
        
        UserDefaults.standard.setValue(exportAllAlarmsID, forKey: "allAlarmsID")
        UserDefaults.standard.setValue(exportAllAlarmsTime, forKey: "allAlarmsTime")
        UserDefaults.standard.setValue(exportAllAlarmsSwitches, forKey: "allAlarmsSwitches")
        updateActiveAlarms(allAlarms: self.allAlarms)
        }
    
    func updateActiveAlarms (allAlarms: [Alarm]){
        self.activeAlarms.removeAll()
        for alarm in allAlarms {
            if alarm.isOn == true{
                self.activeAlarms.append(alarm)
            }
        }
        
        if activeAlarms.isEmpty {
            UserDefaults.standard.setValue(nil, forKey: "nextAlarmTime")
            print("No active alarms")
            return
        }
        
        
        for alarmIndex in 0..<activeAlarms.count-1{
            var startingIndex = alarmIndex + 1
            for walkingIndex in startingIndex..<activeAlarms.count{
                
                if Int(activeAlarms[walkingIndex].time.prefix(2))! < Int(activeAlarms[alarmIndex].time.prefix(2))! {
                    swapArraysElements(index1: alarmIndex, index2: walkingIndex)
                }else{
                    if Int(activeAlarms[walkingIndex].time.prefix(2))! == Int(activeAlarms[alarmIndex].time.prefix(2))!{
                        if Int(activeAlarms[walkingIndex].time.suffix(2))! < Int(activeAlarms[alarmIndex].time.suffix(2))!{
                            swapArraysElements(index1: alarmIndex, index2: walkingIndex)
                        }
                    }
                }
            }
            
            
        }
        
        activateNearestAlarm(sortedActiveAlarms: self.activeAlarms)
    }
    
    func activateNearestAlarm (sortedActiveAlarms: [Alarm]){
        var currentHour = Calendar.current.component(.hour, from: Date())
        var currentMinutes = Calendar.current.component(.minute, from: Date())
        var alarmToActivate = sortedActiveAlarms[0]
        for index in 0..<sortedActiveAlarms.count{
            
            if Int(activeAlarms[index].time.prefix(2))! > currentHour {
                alarmToActivate = activeAlarms[index]
                break
            }else{
                if Int(activeAlarms[index].time.prefix(2))! == currentHour{
                    if Int(activeAlarms[index].time.suffix(2))! > currentMinutes{
                        alarmToActivate = activeAlarms[index]
                        break
                    }
                }
            }
            
        }
        
        var currentAlarmTime = UserDefaults.standard.object(forKey: "nextAlarmTime") as? String
        
        if currentAlarmTime == nil {
            print("FIRST ALARM SET")
            UserDefaults.standard.setValue(alarmToActivate.time, forKey: "nextAlarmTime")
            AppDelegate.sendNotification(alarmID: alarmToActivate.alarmID, melody: alarmToActivate.melody, repeatsMinutesIntervals: alarmToActivate.timeInterval, alarmTime: alarmToActivate.time)
        }else{
            //AppDelegate.notificationCenter.getNotificationSettings(completionHandler: @escaping (UNNotificationSettings) -> Void)
            AppDelegate.notificationCenter.getPendingNotificationRequests { (scheduledNotifications) in
                if scheduledNotifications.count > 0 && currentAlarmTimeIsLess(currentAlarmTime: currentAlarmTime!, alarmToActivate: alarmToActivate) && currentAlarmIsNotTooOld(currentHour: currentHour, currentMinutes: currentMinutes, currentAlarmTime: currentAlarmTime!) {
                    
                        print("CURRENT ALARM")
                    
                    
                    // Nearest alarm is not turned off by user yet
                }else{
                    print("ALARM RESET")
                    setCurrentAlarm(alarmToActivate: alarmToActivate)
                }
                
            }
        }
        
        
         
    }
    
    
    func setCurrentAlarm (alarmToActivate: Alarm) {
        UserDefaults.standard.setValue(alarmToActivate.time, forKey: "nextAlarmTime")
        AppDelegate.sendNotification(alarmID: alarmToActivate.alarmID, melody: alarmToActivate.melody, repeatsMinutesIntervals: alarmToActivate.timeInterval, alarmTime: alarmToActivate.time)
    }
    
    func currentAlarmIsNotTooOld (currentHour: Int,currentMinutes: Int,currentAlarmTime: String) -> Bool {
        
        print("BUDABUDABUDA")
        print(currentHour)
        print(currentMinutes)
        print(currentAlarmTime)
        var difference = 0
        if currentHour == Int(currentAlarmTime.prefix(2))! {
            difference = abs(Int(currentAlarmTime.suffix(2))!-currentMinutes)
            if difference > 14{
                print("TOO OLD")
                return false

            }
        }
        if currentHour > Int(currentAlarmTime.prefix(2))! {
            if abs(currentHour - Int(currentAlarmTime.prefix(2))!) > 1 {
                return false
            }else{
                difference = 60 - Int(currentAlarmTime.suffix(2))! + currentMinutes
                if difference > 14{
                    print("TOO OLD")
                    return false
                }
            }
        }
        print("NOT TOO OLD")
        return true
    }
    
    
    func currentAlarmTimeIsLess (currentAlarmTime: String, alarmToActivate: Alarm) -> Bool {
        if  Int(currentAlarmTime.prefix(2))! < Int(alarmToActivate.time.prefix(2))!{
            return true
        }else{
            if Int(currentAlarmTime.prefix(2)) == Int(alarmToActivate.time.prefix(2)){
                if Int(currentAlarmTime.suffix(2))! < Int(alarmToActivate.time.suffix(2))!{
                    return true
                }
                
            }
        
        }
        return false
    }
    
    
    func swapArraysElements(index1: Int,index2: Int){
        print("Active alarms:")
        print(self.activeAlarms)
        var tmp = self.activeAlarms[index1]
        self.activeAlarms[index1] = self.activeAlarms[index2]
        self.activeAlarms[index2] = tmp
    }
}

struct Alarms_Previews: PreviewProvider {
    static var previews: some View {
        Alarms()
    }
}

class Alarm: Identifiable {
    init(id:Int, time: String,alarmID: Int,alarmDate: String, isOn: Bool, melody: String, timeInterval: Int){
        self.id = id
        self.time = time
        self.isOn = isOn
        self.alarmID = alarmID
        self.alarmDate = alarmDate
        self.timeInterval = timeInterval
        self.melody = melody
    }
    var id: Int
    var time: String
    var isOn: Bool
    var alarmID: Int
    var alarmDate: String
    var melody: String
    var timeInterval: Int
}
