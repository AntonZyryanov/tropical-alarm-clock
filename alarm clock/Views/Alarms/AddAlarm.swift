//
//  AddAlarm.swift
//  alarm clock
//
//  Created by TonyMontana on 21.10.2021.
//

import SwiftUI



struct AddAlarm: View {
    
    
 /*   init() {
        UIDatePicker.appearance().tintColor = UIColor.purple
        UIDatePicker.appearance().backgroundColor = .white
    }   */
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @State var chosenTime = Date.tomorrow
    
    @Binding var allAlarms: [Alarm]
    var minutes = [2,3,4,5,6,7,8,9,10]
        @State  var selectedInterval = 5
    
    @Binding var switchesFix: [Bool]
    
    @State var showingPermissionAlert: Bool = false
    
    var body: some View {
        ZStack{
            Color.black.ignoresSafeArea()
            VStack {
                HStack{
                    Spacer().frame(width: 20)
                    Button(action: {
                        presentationMode.wrappedValue.dismiss()
                                    }) {
                        HStack{
                            Image(systemName: "arrow.left")
                                .foregroundColor(.purple)
                            Spacer().frame(width: 8)
                        }
                        Spacer().frame(height: 40)
                        
                }
                    Spacer()
                    
                    Button(action: {
                        
                    
                        
                        var alarmIdentifier = UserDefaults.standard.object(forKey: "alarmIdentifier") as? Int
                        
                        if alarmIdentifier == nil {
                            alarmIdentifier = 0
                        }else {
                            alarmIdentifier! += 1
                        }
                        
                        var melody = UserDefaults.standard.object(forKey: "melody") as? String
                        
                        if melody == nil {
                            melody = "tropic"
                        }
                        
                        let hours = Calendar.current.component(.hour, from: chosenTime)
                        var hoursString = ""
                        if hours < 10 {
                            hoursString = "0" + String(hours)
                        }else{
                            hoursString = String(hours)
                        }
                        let minutes = Calendar.current.component(.minute, from: chosenTime)
                        var minutesString = ""
                        if minutes < 10 {
                            minutesString = "0" + String(minutes)
                        }else{
                            minutesString = String(minutes)
                        }
                        let time = hoursString + ":" + minutesString
                        
                        
                        let alarmDate = String(Calendar.current.component(.day, from: chosenTime)) + "." + String(Calendar.current.component(.month, from: chosenTime)) + "." + String(Calendar.current.component(.year, from: chosenTime))
                        
                        let newAlarmID = allAlarms.count
                        allAlarms.append(Alarm(id: newAlarmID, time: time, alarmID: alarmIdentifier!, alarmDate: alarmDate,isOn: true,melody: melody!,timeInterval: selectedInterval))
                        
                        allAlarms.last?.isOn = true
                        switchesFix.append(true)
                        
                        //
                        var exportAllAlarmsID : [Int] = []
                        var exportAllAlarmsTime : [String] = []
                        var exportAllAlarmsSwitches : [Bool] = []
                        var exportAllAlarmsAlarmsID : [Int] = []
                        var exportAllAlarmsAlarmsDate : [String] = []
                        var exportAllAlarmsMelodies : [String] = []
                        var exportAllAlarmsTimeIntervals : [Int] = []
                        for index in 0..<allAlarms.count {
                            exportAllAlarmsID.append(allAlarms[index].id)
                            exportAllAlarmsTime.append(allAlarms[index].time)
                            exportAllAlarmsSwitches.append(allAlarms[index].isOn)
                            exportAllAlarmsAlarmsID.append(allAlarms[index].alarmID)
                            exportAllAlarmsAlarmsDate.append(allAlarms[index].alarmDate)
                            exportAllAlarmsMelodies.append(allAlarms[index].melody)
                            exportAllAlarmsTimeIntervals.append(allAlarms[index].timeInterval)
                        }
                        UserDefaults.standard.setValue(alarmIdentifier, forKey: "alarmIdentifier")
                        
                        UserDefaults.standard.setValue(exportAllAlarmsID, forKey: "allAlarmsID")
                        UserDefaults.standard.setValue(exportAllAlarmsTime, forKey: "allAlarmsTime")
                        UserDefaults.standard.setValue(exportAllAlarmsSwitches, forKey: "allAlarmsSwitches")
                        UserDefaults.standard.setValue(exportAllAlarmsAlarmsID, forKey: "allAlarmsAlarmsID")
                        
                        UserDefaults.standard.setValue(exportAllAlarmsAlarmsDate, forKey: "allAlarmsAlarmsDate")
                        
                        UserDefaults.standard.setValue(exportAllAlarmsMelodies, forKey: "allAlarmsAlarmsMelodies")
                        UserDefaults.standard.setValue(exportAllAlarmsTimeIntervals, forKey: "allAlarmsAlarmsTimeIntervals")
                        
                        //
                        
                        
                        
                     //   AppDelegate.sendNotification(alarmID: alarmIdentifier!, melody: melody!, repeatsMinutesIntervals: selectedInterval, alarmTime: time)
                        presentationMode.wrappedValue.dismiss()
                        print("Alarm added")
                        
                        }) {
                        HStack{
                            Image(systemName: "plus")
                                .foregroundColor(.purple)
                            Spacer().frame(width: 8)
                        }
                     //   Spacer().frame(height: 40)
                        
                }
                    Spacer().frame(width: 20)
                }
                Spacer().frame(height: 60)
                Image("littleLogo")
                Spacer().frame(height:120)
        
                
                HStack{
                    VStack{
                        Text("TIME").foregroundColor(Color.purple)
                            .font(Font.custom("DINCondensed-Bold", size: 20))
                            .shadow(color: .purple, radius: 6, x: -1, y: -1)
                            .offset(x: 75,y: 78)
                        
                        DatePicker(selection: $chosenTime, in: Date()...Calendar.current.date(byAdding: .year, value: 1, to: Date())!, displayedComponents: .hourAndMinute) {
                                    Text("")
                                        
                                }.foregroundColor(Color.purple).datePickerStyle(WheelDatePickerStyle.init())
                        .frame(width: 150 , height: 150)
                        .applyTextColor(Color.purple).offset(x: -32,y: -20)
                    }
                   
                    VStack{
                        Text("REPEAT INTERVAL").foregroundColor(Color.purple)
                            .font(Font.custom("DINCondensed-Bold", size: 20))
                            .shadow(color: .purple, radius: 6, x: -1, y: -1)
                            .offset(x: -40,y: 140)
                        Picker("", selection: $selectedInterval) {
                                        ForEach(minutes, id: \.self) {
                                            Text(String($0))
                                        }
                                    }
                        .foregroundColor(Color.purple).datePickerStyle(WheelDatePickerStyle.init())
                .frame(width: 50 , height: 150)
                        .offset(x: 40,y: 40)
                .applyTextColor(Color.purple)
                        
                    }
                }
                
                Spacer()

                        
                    }
            
        }
        .navigationBarTitle("")
      .navigationBarHidden(true)
        .onAppear{
            
        }
        .alert(isPresented:$showingPermissionAlert) {
                
            Alert(title: Text("Please turn Notifications on in Settings to use alarms"),
                message: Text(""),
                dismissButton: Alert.Button.default(
                    Text("Go to settings"), action: { UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!) }
                )
            )
            }
    }
    
    
}

struct AddAlarm_Previews: PreviewProvider {
    static var previews: some View {
        //AddAlarm()
        Text("")
    }
}


extension View {
  @ViewBuilder func applyTextColor(_ color: Color) -> some View {
    if UITraitCollection.current.userInterfaceStyle == .light {
      self.colorInvert().colorMultiply(color)
    } else {
      self.colorMultiply(color)
    }
  }
}


extension Date {
    static var yesterday: Date { return Date().dayBefore }
    static var tomorrow:  Date { return Date().dayAfter }
    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return dayAfter.month != month
    }
}
