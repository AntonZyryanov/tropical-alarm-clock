//
//  savedTimers.swift
//  alarm clock
//
//  Created by TonyMontana on 20.10.2021.
//

import SwiftUI

struct SavedTimers: View {
    

    
    
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @Binding var savedTimersArray: [SavedTimer]
    
    var body: some View {
        ZStack{
            Color.black.ignoresSafeArea()
            //VStack{
                
                
                ScrollView{
                    
                    HStack{
                        Spacer().frame(width: 20)
                        Button(action: {
                            presentationMode.wrappedValue.dismiss()
                                        }) {
                            HStack{
                                Image(systemName: "arrow.left")
                                    .foregroundColor(.purple)
                                Spacer().frame(width: 8)
                            }
                            Spacer()
                    }
                    }.frame(height: 60)
                    
                    Spacer().frame(height: 25)
                    
                    HStack{
                       // Image("stripperLittleLogo")
                        Spacer().frame(width: 20)
                        VStack
                        {
                        Text("SAVED RECORDINGS").foregroundColor(Color.purple)
                            .font(Font.custom("DINCondensed-Bold", size: 60))
                            .shadow(color: .purple, radius: 6, x: -1, y: -1)
                        Spacer()
                            }
                        Spacer()
                        VStack{
                            Image("wineNew")
                            Spacer()
                        }
                        
                    }.frame(height:130)   
                    
                    ForEach(savedTimersArray,id:\.id){ timer in
                        VStack{
                           // Spacer()
                            Spacer().frame(height:10)
                            
                            HStack{
                                Spacer().frame(width:20)
                                VStack{
                                    Text(timer.time).foregroundColor(.purple)
                                        .foregroundColor(Color.purple)
                                        .font(Font.custom("DINCondensed-Bold", size: 40))
                                        .shadow(color: .purple, radius: 6, x: -1, y: -1)
                                    Spacer().frame(height:5)
                                    HStack{
                                        Spacer().frame(width:5)
                                        Text(timer.date).foregroundColor(.purple)
                                            .foregroundColor(Color.purple)
                                            .font(Font.custom("DINCondensed-Bold", size: 15))
                                            .shadow(color: .purple, radius: 6, x: -1, y: -1)
                                    }
                                    
                                }
                                
                                Spacer()
                                VStack{
                                    HStack{
                                        Spacer()
                                        Button(action: {
                                            var newTimerID = UserDefaults.standard.object(forKey: "newTimerID") as? Int
                                            
                                            let deletedElement = timer.id
                                            savedTimersArray.remove(at: timer.id)
                                            
                                            for timer in savedTimersArray {
                                                if timer.id > deletedElement{
                                                    timer.id -= 1
                                                }
                                                
                                            }
                                            
                                            var exportTimersID : [Int] = []
                                            var exportTimersTimes : [String] = []
                                            var exportTimersDates : [String] = []
                                            for index in 0..<savedTimersArray.count {
                                                exportTimersID.append(savedTimersArray[index].id)
                                                exportTimersTimes.append(savedTimersArray[index].time)
                                                exportTimersDates.append(savedTimersArray[index].date)
                                            }
                                            if newTimerID != nil {
                                                newTimerID! -= 1
                                            }
                                            
                                            UserDefaults.standard.setValue(exportTimersTimes, forKey: "savedTimersTimes")
                                            UserDefaults.standard.setValue(exportTimersDates, forKey: "savedTimersDates")
                                            UserDefaults.standard.setValue(exportTimersID, forKey: "savedTimersID")
                                            UserDefaults.standard.setValue(newTimerID, forKey: "newTimerID")
                                                             
                                                                         }) {
                                                             HStack{
                                                              Image(systemName: "trash.circle")
                                                                     .foregroundColor(.purple)
                                                                 Spacer().frame(width: 8)
                                                             }.frame(minWidth: 0, maxWidth: 40, minHeight: 0, maxHeight: 40)
                                                             
                                                             
                                                         }.frame(width: 40, height: 40)
                                    }
                                    
                                    Spacer().frame(height:20)
                                    
                                    
                                }
                                
                                Spacer().frame(width:20)
                            }
                           // Spacer().frame(height:10)
                        }
                        
                        
                        
                        
                   // }.listRowBackground(Color.black)
                }
                Spacer()
                
            }.navigationBarTitle("")
            .navigationBarHidden(true)
        }
    }
}

struct savedTimers_Previews: PreviewProvider {
    static var previews: some View {
        //savedTimers(datesAndTimes: )
        Text("")
    }
}


