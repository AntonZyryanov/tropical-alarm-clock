//
//  Timer.swift
//  alarm clock
//
//  Created by TonyMontana on 19.10.2021.
//

import SwiftUI

struct Timers: View {
    
    init(){
        UITableView.appearance().backgroundColor = .black
        UITableView.appearance().rowHeight = 100
        UITabBar.appearance().isHidden = true
        UITabBar.appearance().barTintColor = UIColor.purple
        UIBarButtonItem.appearance().tintColor = UIColor.purple
        UITabBar.appearance().backgroundColor = UIColor.black
        UINavigationBar.appearance().backgroundColor = UIColor.black
        UINavigationBar.appearance().barTintColor = UIColor.black
        
    }
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @State var timer = Timer.publish(every: 0.1, on: .main, in: .common).autoconnect()
    
    @State var secondsFirstDigit: Int = 0
    @State var secondsSecondDigit: Int = 0//"00:00"
    @State var miliseconds: Int = 0
    @State var minutesFirstDigit: Int = 0
    @State var minutesSecondDigit: Int = 0
    @State var hoursFirstDigit: Int = 0
    @State var hoursSecondDigit: Int = 0
    @State var isJustOpened: Bool = true
    @State var savedTimersArray: [SavedTimer] = []
    @State var savedTimersNewId: Int = 0
    
    var body: some View {
        ZStack{
            
            Color.black.ignoresSafeArea()
            
            VStack{
                HStack{
                    Spacer().frame(width: 20,height: 40)
                    Button(action: {
                        presentationMode.wrappedValue.dismiss()
                                    }) {
                        HStack{
                            Image(systemName: "arrow.left")
                                .foregroundColor(.purple)
                            Spacer().frame(width: 8)
                        }
                        Spacer().frame(height: 40)
                }
                    Spacer().frame(height: 40)
                    NavigationLink(destination: SavedTimers(savedTimersArray: $savedTimersArray)){
                        ZStack{
                            Image(systemName: "tray.full.fill")
                                .foregroundColor(.purple)
                        }
                        
                    }
                    
                    Spacer().frame(width: 20,height: 40)
                }.frame(height: 40)
                Spacer().frame(height: 40)
                HStack{
                   // Image("stripperLittleLogo")
                    Spacer().frame(width: 20)
                    VStack
                    {
                    Text("STOPWATCH").foregroundColor(Color.purple)
                        .font(Font.custom("DINCondensed-Bold", size: 60))
                        .shadow(color: .purple, radius: 6, x: -1, y: -1)
                    Spacer()
                        }
                    Spacer()
                    VStack{
                        Image("stopwatch")
                        Spacer().frame(height: 110)
                    }
                    Spacer().frame(width: 10)
                }.frame(height:180)
                 Spacer().frame(height: 40)
                //Text("2222")
                HStack{
                    Text( ( String(hoursFirstDigit) + String(hoursSecondDigit)) + ":" )
                    .foregroundColor(.purple)
                    .font(Font.custom("DBLCDTempBlack", size: 60))
                    .shadow(color: .purple, radius: 6, x: -1, y: -1)
                    Text( ( String(minutesFirstDigit) + String(minutesSecondDigit)) + ":" )
                    .foregroundColor(.purple)
                    .font(Font.custom("DBLCDTempBlack", size: 60))
                    .shadow(color: .purple, radius: 6, x: -1, y: -1)
                    Text( ( String(secondsFirstDigit) + String(secondsSecondDigit)) + ":" + String(miliseconds) )
                    .foregroundColor(.purple)
                    .font(Font.custom("DBLCDTempBlack", size: 60))
                    .shadow(color: .purple, radius: 6, x: -1, y: -1)
                }
                
                .onReceive(timer) { input in
                    if isJustOpened == false{
                        self.miliseconds += 1
                        if self.miliseconds == 10{
                            self.secondsSecondDigit += 1
                            self.miliseconds = 0
                        }
                        if self.secondsSecondDigit == 10{
                            self.secondsFirstDigit += 1
                            self.secondsSecondDigit = 0
                        }
                        if self.secondsFirstDigit == 6 {
                            self.minutesSecondDigit += 1
                            self.secondsFirstDigit = 0
                        }
                    if self.minutesSecondDigit == 10{
                        self.minutesFirstDigit += 1
                        self.minutesSecondDigit = 0
                    }
                    if self.minutesFirstDigit == 6 {
                        self.hoursSecondDigit += 1
                        self.minutesFirstDigit = 0
                    }
                    if self.hoursSecondDigit == 10{
                        self.hoursFirstDigit += 1
                        self.hoursSecondDigit = 0
                    }
                    }
                                }
               // Spacer().frame(height: 250)
                Spacer()
                HStack{
                    Button(action: {
                        self.timer.upstream.connect().cancel()
                        self.miliseconds = 0
                        self.secondsSecondDigit = 0
                        self.secondsFirstDigit = 0
                        self.minutesSecondDigit = 0
                        self.minutesFirstDigit = 0
                        self.hoursSecondDigit = 0
                        self.hoursFirstDigit = 0
                                    }) {
                            Image(systemName: "gobackward")
                                .foregroundColor(.purple)
                }
                    Spacer().frame(width: 40)
                    
                    Button(action: {
                        self.isJustOpened = false
                        timer = Timer.publish(every: 0.1, on: .main, in: .common).autoconnect()
                                    }) {
                            Image(systemName: "play.fill")
                                .foregroundColor(.purple)
                }
                    Spacer().frame(width: 40)
                    Button(action: {
                        self.timer.upstream.connect().cancel()
                                    }) {
                            Image(systemName: "pause.fill")
                                .foregroundColor(.purple)
                }
                    Spacer().frame(width: 40)
                    Button(action: {
                        var newTimerID = UserDefaults.standard.object(forKey: "newTimerID") as? Int
                        if newTimerID == nil {
                            newTimerID = 0
                        }else{
                            newTimerID! += 1
                        }   
                        if savedTimersArray.count > 0 {
                            var newTimerID = savedTimersArray.count + 1
                        }
                        
                        let timeAndDate = TimersFunctionality.saveTime(hoursFirstDigit: hoursFirstDigit, hoursSecondDigit: hoursSecondDigit, minutesFirstDigit: minutesFirstDigit, minutesSecondDigit: minutesSecondDigit, secondsFirstDigit: secondsFirstDigit, secondsSecondDigit: secondsSecondDigit, miliseconds: miliseconds)
                      //  self.savedTimesArray.updateValue(timeAndDate.0, forKey: timeAndDate.1)
                        self.savedTimersArray.append(SavedTimer(id: newTimerID!, time: timeAndDate.0, date: timeAndDate.1))
                        self.savedTimersNewId += 1
                        
                        
                        var exportTimersID : [Int] = []
                        var exportTimersTimes : [String] = []
                        var exportTimersDates : [String] = []
                        for index in 0..<savedTimersArray.count {
                            exportTimersID.append(savedTimersArray[index].id)
                            exportTimersTimes.append(savedTimersArray[index].time)
                            exportTimersDates.append(savedTimersArray[index].date)
                        }
                        UserDefaults.standard.setValue(exportTimersTimes, forKey: "savedTimersTimes")
                        UserDefaults.standard.setValue(exportTimersDates, forKey: "savedTimersDates")
                        UserDefaults.standard.setValue(exportTimersID, forKey: "savedTimersID")
                        UserDefaults.standard.setValue(newTimerID, forKey: "newTimerID")
                        
                                    }) {
                            Image(systemName: "tray.and.arrow.down.fill")
                                .foregroundColor(.purple)
                }
                }
                Spacer().frame(height:20)
            }
        }.navigationBarTitle("")
        .navigationBarHidden(true)
        .onAppear {
            AlarmAnalytics.analyticsEvent(event: AlarmAnalytics.Events.stopwatchShown.rawValue)
            savedTimersArray.removeAll()
            
            let savedTimersTimes = UserDefaults.standard.object(forKey: "savedTimersTimes") as? [String]
            let savedTimersDates = UserDefaults.standard.object(forKey: "savedTimersDates") as? [String]
            let savedTimersID = UserDefaults.standard.object(forKey: "savedTimersID") as? [Int]
           
            if savedTimersTimes != nil {
                for index in 0..<savedTimersTimes!.count {
                    savedTimersArray.append(SavedTimer(id: savedTimersID![index], time: savedTimersTimes![index], date: savedTimersDates![index]))
                }
            }
        }
        .onDisappear {
            
         
        }
    }
}

struct Timer_Previews: PreviewProvider {
    static var previews: some View {
        Timers()
    }
}

class TimersFunctionality {
    func displayTimer (hoursFirstDigit: Int, hoursSecondDigit: Int, minutesFirstDigit: Int, minutesSecondDigit: Int, secondsFirstDigit: Int, secondsSecondDigit: Int, miliseconds: Int){
        
    }
    static func saveTime (hoursFirstDigit: Int, hoursSecondDigit: Int, minutesFirstDigit: Int, minutesSecondDigit: Int, secondsFirstDigit: Int, secondsSecondDigit: Int, miliseconds: Int ) -> (String,String) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .medium
        let date = dateFormatter.string(from: Date())
        let time = String(hoursFirstDigit) + String(hoursSecondDigit) + ":" + String(minutesFirstDigit) + String(minutesSecondDigit) + ":" + String(secondsFirstDigit) + String(secondsSecondDigit)
        return (time,date)
    }
}


class SavedTimer: Identifiable {
    init(id:Int, time: String, date: String){
        self.id = id
        self.time = time
        self.date = date
    }
    var id: Int
    var time: String
    var date: String
}
