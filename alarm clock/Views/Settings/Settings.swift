//
//  Setting.swift
//  alarm clock
//
//  Created by TonyMontana on 25.10.2021.
//

import SwiftUI
import StoreKit

struct Settings: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @State private var showingAlertAppStore = false
    
    @State private var showTermsAndConditions: Bool = false
    
    @State private var isPrvacyPolicyChosen: Bool = false
    
    @State var allSettings: [Setting] = [Setting(id: 0, name: "SUBSCRIPTION",text: "SUBSCRIBE NOW"),
                                         Setting(id: 1, name: "FEEDBACK",text: "ANZY.MOBDEV@GMAIL.COM"),
                                         Setting(id: 2, name: "APPSTORE",text: "COPY APPSTORE LINK"),
                                         Setting(id: 3, name: "RATE", text: "RATE APP"),
                                         Setting(id: 4, name: "LEGAL",text: "READ DOCUMENTATION"),
    ]
    
    var body: some View {
        ZStack{
            
            Color.black.ignoresSafeArea()
            
            VStack{
                Spacer().frame(height: 40)
                HStack{
                    Spacer().frame(width: 20)
                    Button(action: {
                        presentationMode.wrappedValue.dismiss()
                                    }) {
                        HStack{
                            Image(systemName: "arrow.left")
                                .foregroundColor(.purple)
                            Spacer().frame(width: 8)
                        }
                        Spacer().frame(height: 40)
                }
                    Spacer()
                    
                    Spacer().frame(width: 20)
                }
                Spacer().frame(height: 25)
                HStack{
                   // Image("stripperLittleLogo")
                    Spacer().frame(width: 20)
                    VStack
                    {
                    Text("SETTINGS").foregroundColor(Color.purple)
                        .font(Font.custom("DINCondensed-Bold", size: 60))
                        .shadow(color: .purple, radius: 6, x: -1, y: -1)
                    Spacer()
                        }
                    Spacer()
                    VStack{
                        Image("stripper")
                        Spacer()
                    }
                    
                }
                VStack{
                    Spacer().frame(height: 10)
                    
                    VStack{
                        HStack{
                            Spacer().frame(width: 20)
                            Text("SUBSCRIPTION")
                                .foregroundColor(Color.purple)
                                .font(Font.custom("DINCondensed-Bold", size: 30))
                                .shadow(color: .purple, radius: 6, x: -1, y: -1)
                            Spacer()
                        }
                        Spacer().frame(height: 20)
                        HStack{
                            //Spacer().frame(width: 100)
                            Spacer()
                            NavigationLink(destination: Paywall()){
                                Text("SUBSCRIBE NOW")
                                    .foregroundColor(Color.purple)
                                    .font(Font.custom("DINCondensed-Bold", size: 20))
                                    .shadow(color: .purple, radius: 6, x: -1, y: -1)
                            }
                            
                                
                            Spacer().frame(width: 40)
                        }
                        
                        Spacer().frame(height: 20)
                    }
                    
                    Spacer().frame(height: 10)
                    
                    VStack{
                        HStack{
                            Spacer().frame(width: 20)
                            Text("FEEDBACK")
                                .foregroundColor(Color.purple)
                                .font(Font.custom("DINCondensed-Bold", size: 30))
                                .shadow(color: .purple, radius: 6, x: -1, y: -1)
                            Spacer()
                        }
                        Spacer().frame(height: 10)
                        HStack{
                            //Spacer().frame(width: 100)
                            Spacer()
                            Link(destination: URL(string: "mailto:anzy.mobdev@gmail.com")!) {
                                Text("ANZY.MOBDEV@GMAIL.COM")
                                    .foregroundColor(Color.purple)
                                    .font(Font.custom("DINCondensed-Bold", size: 20))
                                    .shadow(color: .purple, radius: 6, x: -1, y: -1)
                            }
                                
                            Spacer().frame(width: 40)
                        }
                        
                        Spacer().frame(height: 10)
                    }
                    
                    Spacer().frame(height: 10)
                    
                    VStack{
                        HStack{
                            Spacer().frame(width: 20)
                            Text("APPSTORE")
                                .foregroundColor(Color.purple)
                                .font(Font.custom("DINCondensed-Bold", size: 30))
                                .shadow(color: .purple, radius: 6, x: -1, y: -1)
                            Spacer()
                        }
                        Spacer().frame(height: 10)
                        HStack{
                            //Spacer().frame(width: 100)
                            Spacer()
                            
                            Button(action: {
                                UIPasteboard.general.string = "https://www.apple.com/ru/app-store/"
                                let appStoreLink = UserDefaults.standard.object(forKey: "appStoreLink") as? String
                                if appStoreLink != nil{
                                    UIPasteboard.general.string = appStoreLink
                                }
                                
                                
                                showingAlertAppStore = true
                                            }) {
                                
                                Text("COPY APPSTORE LINK")
                                    .foregroundColor(Color.purple)
                                    .font(Font.custom("DINCondensed-Bold", size: 20))
                                    .shadow(color: .purple, radius: 6, x: -1, y: -1)
                                
                        }
                            
                                
                            Spacer().frame(width: 40)
                        }
                        
                        Spacer().frame(height: 10)
                    }
                    
                    Spacer().frame(height: 10)
                    
                    VStack{
                        HStack{
                            Spacer().frame(width: 20)
                            Text("RATE")
                                .foregroundColor(Color.purple)
                                .font(Font.custom("DINCondensed-Bold", size: 30))
                                .shadow(color: .purple, radius: 6, x: -1, y: -1)
                            Spacer()
                        }
                        Spacer().frame(height: 10)
                        HStack{
                            //Spacer().frame(width: 100)
                            Spacer()
                            
                            Button(action: {
                              //  SKStoreReviewController.requestReview()
                                //
                                if let windowScene = UIApplication.shared.connectedScenes.first(where: {$0.activationState == .foregroundActive}) as? UIWindowScene { SKStoreReviewController.requestReview(in: windowScene) }
                                            }) {
                                
                                Text("RATE APP")
                                    .foregroundColor(Color.purple)
                                    .font(Font.custom("DINCondensed-Bold", size: 20))
                                    .shadow(color: .purple, radius: 6, x: -1, y: -1)
                                
                        }
                            
                                
                            Spacer().frame(width: 40)
                        }
                        
                        Spacer().frame(height: 10)
                    }
                    
                    Spacer().frame(height: 10)
                    
                    VStack{
                        HStack{
                            Spacer().frame(width: 20)
                            Text("LEGAL")
                                .foregroundColor(Color.purple)
                                .font(Font.custom("DINCondensed-Bold", size: 30))
                                .shadow(color: .purple, radius: 6, x: -1, y: -1)
                            Spacer()
                        }
                        Spacer().frame(height: 10)
                        HStack{
                            //Spacer().frame(width: 100)
                            Spacer()
                            
                            Button(action: {
                                self.isPrvacyPolicyChosen = true
                                    self.showTermsAndConditions = true
                                            }) {
                                
                                Text("PRIVACY POLICY")
                                    .foregroundColor(Color.purple)
                                    .font(Font.custom("DINCondensed-Bold", size: 20))
                                    .shadow(color: .purple, radius: 6, x: -1, y: -1)
                                
                        }
                            
                                
                            Spacer().frame(width: 40)
                        }
                        
                        Spacer().frame(height: 10)
                        HStack{
                            //Spacer().frame(width: 100)
                            Spacer()
                            
                            Button(action: {
                                self.isPrvacyPolicyChosen = false
                                    self.showTermsAndConditions = true
                                            }) {
                                
                                Text("TERMS AND CONDITIONS")
                                    .foregroundColor(Color.purple)
                                    .font(Font.custom("DINCondensed-Bold", size: 20))
                                    .shadow(color: .purple, radius: 6, x: -1, y: -1)
                                
                        }
                            
                                
                            Spacer().frame(width: 40)
                        }
                        
                        Spacer().frame(height: 10)
                    }
                    
             
                    
                }.background(Color.black)
                .frame(height:500)
                Spacer().frame(height:20)
            }
            
        }.navigationBarTitle("")
        .navigationBarHidden(true)
        .alert(isPresented: $showingAlertAppStore) {
                    Alert(title: Text("AppStore link copied to clipboard"), message: Text(""), dismissButton: .default(Text("Continue")))
                }
        .fullScreenCover(isPresented: $showTermsAndConditions, content: {
           // SFSafariViewWrapper(url: URL(string: self.link)!)
            Browser(isPrvacyPolicyChosen: $isPrvacyPolicyChosen)
        })
        .onAppear{
            AlarmAnalytics.analyticsEvent(event: AlarmAnalytics.Events.settingsShown.rawValue)
            
        }
        
    }
}

struct Settings_Previews: PreviewProvider {
    static var previews: some View {
        Settings()
    }
}


class Setting: Identifiable {
    init(id:Int,name: String,text:String){
        self.id = id
        self.name = name
        self.text = text
    }
    var id: Int
    var name: String
    var text: String
}
