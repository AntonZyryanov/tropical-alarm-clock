//
//  OnBoardingBG.swift
//  alarm clock
//
//  Created by TonyMontana on 24.11.2021.
//

import SwiftUI

struct OnBoardingBG: View {
    @Binding var onBoardingID: Int
    var body: some View {
        if onBoardingID == 0{
            Image("OnBoardingBG")
        }else{
            if onBoardingID == 1{
                Image("OnBoardingBG2")
            }else{
                Image("OnBoardingBG3")
            }
        }
    }
}

struct OnBoardingBG_Previews: PreviewProvider {
    static var previews: some View {
        Text("")
    }
}
