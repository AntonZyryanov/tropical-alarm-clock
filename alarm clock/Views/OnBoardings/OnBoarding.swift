//
//  OnBoarding.swift
//  alarm clock
//
//  Created by TonyMontana on 23.11.2021.
//

import SwiftUI
import AppTrackingTransparency

struct OnBoarding: View {
    
    
    @State var onBoardingID: Int = 0
    
    @State var backgroundImage: String = "OnBoardingBG"
    
    @State var paywallShown: Bool? = UserDefaults.standard.object(forKey: "onBoardingPassed") as? Bool
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var body: some View {
        
        if paywallShown == nil {
            if self.onBoardingID == 3 {
                Paywall()
            }else{
                ZStack{
                    Color.black.ignoresSafeArea()
                    VStack{
                        
                        Spacer().frame(height:60)
                        
                        HStack{
                            Spacer().frame(width:20)
                            if onBoardingID == 0{
                                VStack{
                                    Text("HELLO!").foregroundColor(Color.yellow)
                                        .font(Font.custom("DINCondensed-Bold", size: 60))
                                        .shadow(color: .yellow, radius: 6, x: -1, y: -1)                                }
                                
                            }else{
                                if onBoardingID == 1{
                                    Text("FIND YOUR RHYTHM").foregroundColor(Color.yellow)
                                        .font(Font.custom("DINCondensed-Bold", size: 60))
                                        .shadow(color: .yellow, radius: 6, x: -1, y: -1)
                                }else{
                                    Text("FALL ASLEEP IN COMFORT").foregroundColor(Color.yellow)
                                        .font(Font.custom("DINCondensed-Bold", size: 60))
                                        .shadow(color: .yellow, radius: 6, x: -1, y: -1)
                                }
                            }
                            
                            Spacer()
                        }
                        
                        HStack{
                            Spacer().frame(width:22)
                            
                            if onBoardingID == 0{
                                VStack{
                                    Text("WELCOME TO TROPICAL ALARM").foregroundColor(Color.yellow)
                                        .font(Font.custom("DINCondensed-Bold", size: 30))
                                        .shadow(color: .yellow, radius: 6, x: -1, y: -1)
                                    
                                }
                                
                            }else{
                                if onBoardingID == 1{
                                    Text("GET UP ON TIME USING OUR ALARM CLOCK").foregroundColor(Color.yellow)
                                        .font(Font.custom("DINCondensed-Bold", size: 30))
                                        .shadow(color: .yellow, radius: 6, x: -1, y: -1)
                                }else{
                                    Text("USE DREAM HELPERS TO FALL ASLEEP FASTER").foregroundColor(Color.yellow)
                                        .font(Font.custom("DINCondensed-Bold", size: 30))
                                        .shadow(color: .yellow, radius: 6, x: -1, y: -1)
                                }
                            }
                            
                            
                            Spacer()
                        }
                        
                        Spacer()//.frame(height:300)
                        
                        
                        
                            Button(action: {
                                self.onBoardingID += 1
                                if self.onBoardingID == 1 {
                                    AlarmAnalytics.analyticsEvent(event: AlarmAnalytics.Events.secondOnBoardingPassed.rawValue)
                                }
                                if self.onBoardingID == 2 {
                                    AlarmAnalytics.analyticsEvent(event: AlarmAnalytics.Events.thirdOnBoardingPassed.rawValue)
                                }
                              /*  if self.onBoardingID > 2 {
                                    UserDefaults.standard.setValue(true, forKey: "onBoardingPassed")
                                    showPaywall = true
                                    presentationMode.wrappedValue.dismiss()
                                }   */
                                if self.onBoardingID > 2 {
                                    UserDefaults.standard.setValue(true, forKey: "onBoardingPassed")
                                }
                                            }) {
                                ZStack{
                                   /* Capsule()
                                        .fill(Color.pink)
                                        .frame(width: 250, height: 60)  */
                                    VStack{
                                        Spacer().frame(height:50)
                                        Text("CONTINUE").foregroundColor(Color.yellow)
                                            .font(Font.custom("DINCondensed-Bold", size: 40))
                                            .shadow(color: .yellow, radius: 6, x: -1, y: -1)
                                    }
                                    
                                }
                                
                            }
                        
                            
                        
                            
                        
                        
                        
                        Spacer().frame(height:40)
                    }.navigationBarTitle("")
                    .navigationBarHidden(true)
                    .background(
                        OnBoardingBG(onBoardingID: $onBoardingID)
                        )
                    Spacer().frame(height:20)
                    .onAppear{
                        if paywallShown == nil {
                            AlarmAnalytics.analyticsEvent(event: AlarmAnalytics.Events.firstOnBoardingPassed.rawValue)
                        }
                        ATTrackingManager.requestTrackingAuthorization { status in
                            switch status {
                            case .notDetermined:
                                break
                            case .restricted:
                                break
                            case .denied:
                                break
                            case .authorized:
                                break
                            @unknown default:
                                break
                            }
                        }
                        }
                }
            }
        }else{
            Paywall()
        }
        
        
        
        
        
    }
}

struct OnBoarding_Previews: PreviewProvider {
    static var previews: some View {
       // OnBoarding()
        Text("")
    }
}
