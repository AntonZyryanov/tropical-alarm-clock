//
//  ContentView.swift
//  alarm clock
//
//  Created by TonyMontana on 18.10.2021.
//

import SwiftUI
import CoreData
import ApphudSDK

struct ContentView: View {
    
    init() {
        UINavigationBar.appearance().isHidden = true
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) { (allowed, error) in
             //This callback does not trigger on main loop be careful
            if allowed {
              print("Notifications allowed by user") //import os
            } else {
              print("Notifications not allowed by user")
            }
        }
    }
    
    @Environment(\.managedObjectContext) private var viewContext
    
    @State var currentTime: String = Time.getDate()//"00:00"
    
    @State var showPaywall: Bool = false
    
    @State var showOnBoarding: Bool = true
    
    @State var onBoardingPassed: Bool? = false
    
    @State var paywallShown: Bool = false
    
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()

    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \Item.timestamp, ascending: true)],
        animation: .default)
    private var items: FetchedResults<Item>

    var body: some View {
        NavigationView{
        ZStack{
            Color.black.ignoresSafeArea()
            VStack{
                
                HStack{
                    
                    Spacer().frame(height: 40)
                    
                    Spacer().frame(width: 20)
                }.frame(height: 30).padding(0)
              //  Spacer().frame(height: 250)
            /*    Image("logo").frame(width: 250, height: 180, alignment: .center)  */
                Image("littleLogo")
                Spacer().frame(height:120)
                Text(currentTime)
                    .foregroundColor(.purple)
                    //.font(.system(size: 100))
                    .font(Font.custom("DBLCDTempBlack", size: 80))
                    .shadow(color: .purple, radius: 6, x: -1, y: -1)
                    .onReceive(timer) { input in
                        currentTime = Time.getDate()
                                }
                Spacer()
                HStack{
                   // Spacer().frame(height: 40)
                    NavigationLink(destination: Alarms()){
                        ZStack{
                            Image(systemName: "alarm.fill")
                                .foregroundColor(.purple)
                        }
                        
                    }
                    
                    Spacer().frame(width: 40)
                    
                    
                    
                    NavigationLink(destination: Melodies()){
                        ZStack{
                            Image(systemName: "music.note")
                                .foregroundColor(.purple)
                        }
                        
                    }
                    
                    Spacer().frame(width: 40)
                    
                    NavigationLink(destination: DreamHelpers()){
                        ZStack{
                            Image(systemName: "bed.double.fill")
                                .foregroundColor(.purple)
                        }
                        
                    }
                    
                    
                    Spacer().frame(width: 40)
                    NavigationLink(destination: Timers()){
                        ZStack{
                            Image(systemName: "timer")
                                .foregroundColor(.purple)
                        }
                        
                    }
                    Spacer().frame(width: 40)
                    NavigationLink(destination: Settings()){
                        ZStack{
                            Image(systemName: "gear")
                                .foregroundColor(.purple)
                        }
                        
                    }
                    
                   // Spacer().frame(height: 40)
                }.frame(height: 40).padding(0)
                Spacer().frame(height:20)
            }
          }.navigationBarTitle("")
        .navigationBarHidden(true)
      /*  .sheet(isPresented: $showPaywall) {
                    Paywall()
                }   */
        .fullScreenCover(isPresented: $showOnBoarding, content: {
                OnBoarding()
                    .animation(.linear)
            
                
        })
        .onAppear{
            if paywallShown == false{
                paywallShown = true
                self.showOnBoarding = !Apphud.hasActiveSubscription()
            }
            
            RemoteConfigManager.shared.fetchData()
            
            
            
        }
        .navigationViewStyle(StackNavigationViewStyle())
        }.navigationViewStyle(StackNavigationViewStyle())
        
   
    }
    
    
    

    private func addItem() {
        withAnimation {
            let newItem = Item(context: viewContext)
            newItem.timestamp = Date()

            do {
                try viewContext.save()
            } catch {
                
                let nsError = error as NSError
                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
            }
        }
    }

    private func deleteItems(offsets: IndexSet) {
        withAnimation {
            offsets.map { items[$0] }.forEach(viewContext.delete)

            do {
                try viewContext.save()
            } catch {
                
                let nsError = error as NSError
                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
            }
        }
    }
}

private let itemFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .short
    formatter.timeStyle = .medium
    return formatter
}()

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
    }
}

class Time {
    static func getDate () -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .medium
        dateFormatter.dateFormat = "HH:mm:ss"
        return dateFormatter.string(from: Date())
    }
}
