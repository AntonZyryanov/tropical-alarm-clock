//
//  Melodies.swift
//  alarm clock
//
//  Created by TonyMontana on 25.10.2021.
//

import SwiftUI
import ApphudSDK

struct Melodies: View {
    
    init(){
        UITableView.appearance().backgroundColor = .black
        UITableView.appearance().rowHeight = 200
        UITabBar.appearance().isHidden = true
        UITabBar.appearance().barTintColor = UIColor.purple
        UIBarButtonItem.appearance().tintColor = UIColor.purple
        UITabBar.appearance().backgroundColor = UIColor.black
        UINavigationBar.appearance().backgroundColor = UIColor.black
        UINavigationBar.appearance().barTintColor = UIColor.black
        UITableViewCell.appearance().selectionStyle = .none
    }
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @State var showPaywall: Bool = false
    
    @State var chosenMelodyID: Int? = 0
    
    @State var Melodies: [Melody] = [Melody(id: 0, audioTrack:"TROPIC", image: "tape"),
                                     Melody(id: 1, audioTrack:"NINJA", image: "tape"),
                                     Melody(id: 2, audioTrack:"ELEVATOR", image: "tape"),
                                    // Melody(id: 3, audioTrack:"MIDNIGHT", image: "tape"),
                                     Melody(id: 3, audioTrack:"SHAWTY", image: "tape"),
                                     Melody(id: 4, audioTrack:"FROST", image: "tape"),
                                     Melody(id: 5, audioTrack:"TRAP", image: "tape"),
                                     Melody(id: 6, audioTrack:"VICTORY", image: "tape"),
                                     Melody(id: 7, audioTrack:"VENDETTA", image: "tape"),
                                     Melody(id: 8, audioTrack:"DREAMS", image: "tape"),
                                     
    ]
    
    @State var lastSelected: Int = 0
    
    var body: some View {
        ZStack{
            Color.black.ignoresSafeArea()
            ScrollView{
                
                HStack{
                    Spacer().frame(width: 20)
                    Button(action: {
                        AudioPlayer.stopSound()
                        presentationMode.wrappedValue.dismiss()
                                    }) {
                        HStack{
                            Image(systemName: "arrow.left")
                                .foregroundColor(.purple)
                            Spacer().frame(width: 8)
                        }
                        Spacer().frame(height: 40)
                }
                    
                }
                
                Spacer().frame(height: 25)
                HStack{
                   // Image("stripperLittleLogo")
                    Spacer().frame(width: 20)
                    VStack
                    {
                        Spacer().frame(height: 20)
                    Text("MELODIES").foregroundColor(Color.purple)
                        .font(Font.custom("DINCondensed-Bold", size: 60))
                        .shadow(color: .purple, radius: 6, x: -1, y: -1)
                    Spacer()
                        }
                    Spacer()
                    VStack{
                        Image("harp")
                        Spacer().frame(height: 5)
                    }
                    
                }.frame(height:100)
                Spacer().frame(height: 20)
               // List{
                    ForEach(Melodies,id:\.id){ melody in
                        
                        Button(action: {
                            if melody.id > 4 && Apphud.hasActiveSubscription() == false {
                                self.showPaywall = true
                            }else{
                                self.chosenMelodyID = melody.id
                                
                                UserDefaults.standard.setValue(melody.audioTrack.lowercased(), forKey: "melody")
                                UserDefaults.standard.setValue(self.chosenMelodyID, forKey: "melodyID")
                                    if lastSelected != 1000 {
                                        if lastSelected != melody.id{
                                            Melodies[lastSelected].isSelected = false
                                        }
                                    }
                                    melody.isSelected = true
                                    AudioPlayer.playSound(sound: melody.audioTrack.lowercased())
                                    self.lastSelected = melody.id
                            }
                            
                                        }) {
                            HStack{
   
                                Spacer().frame(width: 20)
                                VStack{
                                    Image(melody.image)
                                    Spacer().frame(height:7)
                                }
                                
                                Text(melody.audioTrack)
                                    .foregroundColor(Color.purple)
                                    .font(Font.custom("DINCondensed-Bold", size: 30))
                                    .shadow(color: .purple, radius: 6, x: -1, y: -1)
                                Spacer().frame(height:100)
                                VStack{
                                    if melody.isSelected == false {
                                        Image("checkboxOff")
                                    }else {
                                        Image("checkboxOn")
                                    }
                                    Spacer().frame(height:5)
                                }
                                
                                Spacer().frame(width: 20,height: 100)
                            }
                    }
                        
                   // }.listRowBackground(Color.black)
                    
                }.background(Color.black)
                .frame(height:90)
               // Spacer().frame(height: 40)
                
        }.navigationBarTitle("")
        .navigationBarHidden(true)
        .sheet(isPresented: $showPaywall) {
                        Paywall()
                    }
        .onAppear {
            AlarmAnalytics.analyticsEvent(event: AlarmAnalytics.Events.melodiesShown.rawValue)
            self.chosenMelodyID = UserDefaults.standard.object(forKey: "melodyID") as? Int
            if self.chosenMelodyID == nil {
                self.chosenMelodyID = 0
            }
            Melodies[self.chosenMelodyID!].isSelected = true
            self.lastSelected = self.chosenMelodyID!
            }
            .onDisappear{
                UserDefaults.standard.setValue(self.chosenMelodyID, forKey: "melodyID")
            }
    }
    }
}

struct Melodies_Previews: PreviewProvider {
    static var previews: some View {
        Melodies()
    }
}


class Melody: Identifiable {
    init(id:Int, audioTrack: String,image: String){
        self.id = id
        self.audioTrack = audioTrack
        self.image = image
    }
    var id: Int
    var audioTrack: String
    var image: String
    var isSelected: Bool = false
}
