//
//  DreamHelperAudioPlayer.swift
//  alarm clock
//
//  Created by TonyMontana on 22.10.2021.
//

import SwiftUI
import AVFoundation

struct DreamHelperAudioPlayer: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
  //  @Binding var chosenDreamHelper: String
    
    @State var audioTrack: String = "washing_machine"
    
    init(audio: String) {
        audioTrack = audio
    }
    
    var body: some View {
        ZStack{
            Color.black.ignoresSafeArea()
            VStack{
                Spacer().frame(height: 20)
                HStack{
                    Spacer().frame(width: 20)
                    Button(action: {
                        presentationMode.wrappedValue.dismiss()
                                    }) {
                        HStack{
                            Image(systemName: "arrow.left")
                                .foregroundColor(.purple)
                            Spacer().frame(width: 8)
                        }
                        Spacer().frame(height: 40)
                }
                }
                Spacer().frame(height: 200)
                Image("washing_machine")
                Spacer()
                HStack{
                    Button(action: {
                        AudioPlayer.resumeSound()
                                    }) {
                            Image(systemName: "play.fill")
                                .foregroundColor(.purple)
                }
                    Spacer().frame(width: 200)
                    Button(action: {
                        AudioPlayer.stopSound()
                                    }) {
                            Image(systemName: "pause.fill")
                                .foregroundColor(.purple)
                    }
                    
                }
                Spacer().frame(height: 50)
            }.navigationBarTitle("")
            .navigationBarHidden(true)
            .onAppear {
                AudioPlayer.playSound(sound: audioTrack)
                
            }
        }
    }
}

struct DreamHelperAudioPlayer_Previews: PreviewProvider {
    static var previews: some View {
      //  DreamHelperAudioPlayer()
        Text("zZz")
    }
}


class AudioPlayer {
    static var player: AVAudioPlayer?

    static func playSound(sound: String) {
        guard let url = Bundle.main.url(forResource: sound, withExtension: "mp3") else { return }

        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)

            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)

            /* iOS 10 and earlier require the following line:
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */

            guard let player = player else { return }

            player.numberOfLoops = -1
            player.play()

        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    static func stopSound() {
        player?.pause()
    }
    
    static func resumeSound() {
        player?.play()
    }
}
