//
//  DreamHelpers.swift
//  alarm clock
//
//  Created by TonyMontana on 19.10.2021.
//

import SwiftUI
import ApphudSDK

struct DreamHelpers: View {
    
    
    
    
    init(){
        UITableView.appearance().backgroundColor = .black
        UITableView.appearance().rowHeight = 200
        UITabBar.appearance().isHidden = true
        UITabBar.appearance().barTintColor = UIColor.purple
        UIBarButtonItem.appearance().tintColor = UIColor.purple
        UITabBar.appearance().backgroundColor = UIColor.black
        UINavigationBar.appearance().backgroundColor = UIColor.black
        UINavigationBar.appearance().barTintColor = UIColor.black
        UITableViewCell.appearance().selectionStyle = .none
    }
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @State var chosenDreamHelper: String = "washing_machine"
    
    @State private var isAudioPlayerPresented : Bool = false
    
    @State var lastSelected: Int = 0
    
    @State var showPaywall: Bool = false
    
    @State var allDreamHelpers: [DreamHelper] = [DreamHelper(id: 0, audioTrack:"washing_machine", image: "washing_machine"),
        DreamHelper(id: 1, audioTrack:"rain", image: "rain"),
        DreamHelper(id: 2, audioTrack:"storm", image: "snowflake"),
        DreamHelper(id: 3, audioTrack:"bonfire", image: "bonfire"),
        DreamHelper(id: 4, audioTrack:"sea", image: "sea"),
    ]
    
    var body: some View {
        ZStack{
            Color.black.ignoresSafeArea()
            VStack{
                
               
                ScrollView{
                    HStack{
                        Spacer().frame(width: 20)
                        Button(action: {
                           // AudioPlayer.stopSound()
                            presentationMode.wrappedValue.dismiss()
                                        }) {
                            HStack{
                                Image(systemName: "arrow.left")
                                    .foregroundColor(.purple)
                                Spacer().frame(width: 8)
                            }
                            Spacer().frame(height: 40)
                    }
                        
                    }
                    Spacer().frame(height: 40)
                    HStack{
                       // Image("stripperLittleLogo")
                        Spacer().frame(width: 20)
                        VStack
                        {
                        Text("DREAM HELPERS").foregroundColor(Color.purple)
                            .font(Font.custom("DINCondensed-Bold", size: 60))
                            .shadow(color: .purple, radius: 6, x: -1, y: -1)
                        Spacer()
                            }
                        Spacer()
                        VStack{
                            Image("bedHotel")
                            //Spacer()
                            HStack{
                                Button(action: {
                                    AudioPlayer.playSound(sound: chosenDreamHelper)
                                                }) {
                                        Image(systemName: "play.fill")
                                            .foregroundColor(.purple)
                            }
                                Spacer().frame(width: 20)
                                Button(action: {
                                    AudioPlayer.stopSound()
                                                }) {
                                        Image(systemName: "pause.fill")
                                            .foregroundColor(.purple)
                                }
                                
                            }
                            Spacer().frame(height: 60)
                        }
                        Spacer().frame(width:20)
                        
                    }.frame(height: 150)
                    
                    ForEach(allDreamHelpers,id:\.id){ dreamHelper in
                        
                        Button(action: {
                            if dreamHelper.id > 1 && Apphud.hasActiveSubscription() == false {
                                self.showPaywall = true
                            }else{
                                chosenDreamHelper = dreamHelper.audioTrack
                                if lastSelected != 1000 {
                                    if lastSelected != dreamHelper.id{
                                        allDreamHelpers[lastSelected].isSelected = false
                                    }
                                }
                                dreamHelper.isSelected = true
                                self.lastSelected = dreamHelper.id
                            }
                            
                        }){
                            HStack{
                                Spacer().frame(width:20)
                              /*  Text(dreamHelper.audioTrack).foregroundColor(Color.purple)
                                    .font(Font.custom("Helvetica-Bold ", size: 60))
                                    .shadow(color: .purple, radius: 6, x: -1, y: -1)
                                    .lineLimit(1)
                                    .frame(width: 150)  */
                                Image(dreamHelper.image)
                                Spacer().frame(height:100)
                                VStack{
                                    if dreamHelper.isSelected == false {
                                        Image("checkboxOff")
                                    }else {
                                        Image("checkboxOn")
                                    }
                                    Spacer().frame(height:5)
                                }
                                
                                Spacer().frame(width: 20,height: 100)
                              
                            
                                
                                
                            }
                        }
                        
                        
                    }.listRowBackground(Color.black)
                    
                }.background(Color.black)
                
        }.navigationBarTitle("")
        .navigationBarHidden(true)
            .onAppear(){
                AlarmAnalytics.analyticsEvent(event: AlarmAnalytics.Events.dreamHelpersShown.rawValue)
                allDreamHelpers[0].isSelected = true
            }
        .sheet(isPresented: $showPaywall) {
                            Paywall()
                        }
    }
    }
}

struct DreamHelpers_Previews: PreviewProvider {
    static var previews: some View {
        DreamHelpers()
    }
}


class DreamHelper: Identifiable {
    init(id:Int, audioTrack: String,image: String){
        self.id = id
        self.audioTrack = audioTrack
        self.image = image
    }
    var id: Int
    var audioTrack: String
    var image: String
    var isSelected: Bool = false
}
